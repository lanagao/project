package com.example.yuanann.vegetable_app;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class Back_goodsinsert extends AppCompatActivity {
    String e1,e2,e3,e4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_back_goodsinsert);

        Intent intent = getIntent();
        final int head = intent.getIntExtra("head",0);
        final String t1 = intent.getStringExtra("t1");
        final String t2 = intent.getStringExtra("t2");
        final String t3 = intent.getStringExtra("t3");
        final String t4 = intent.getStringExtra("t4");

        final EditText ed1=(EditText)findViewById(R.id.ed1);
        final EditText ed2=(EditText)findViewById(R.id.ed2);
        final EditText ed3=(EditText)findViewById(R.id.ed3);
        final EditText ed4=(EditText)findViewById(R.id.ed4);
        ImageView image=(ImageView)findViewById(R.id.image);
        Button b1 = (Button) findViewById(R.id.bu1);
        ImageView b2 = (ImageView) findViewById(R.id.ziliao);
        ImageView back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back_goodsinsert.this.finish();
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back_goodsinsert.this.finish();
                String t1 = ed1.getText().toString();
                String t2 = ed2.getText().toString();
                String t3= ed3.getText().toString();
                String t4 = ed4.getText().toString();
                finish();
                Intent intent = new Intent();
                intent.setClass(Back_goodsinsert.this,Fm.class);
                intent.putExtra("t1",t1);
                intent.putExtra("t2",t2);
                intent.putExtra("t3",t3);
                intent.putExtra("t4",t4);
                intent.putExtra("trans","Add Item");
                startActivity(intent);
            }
        });

        if (TextUtils.isEmpty(t1)&&TextUtils.isEmpty(t2)) {

        }else if(TextUtils.isEmpty(t1)){
            ed2.setText(t2);
        }else if(TextUtils.isEmpty(t2)){
            ed1.setText(t1);
        }else{
            ed1.setText(t1);
            ed2.setText(t2);
        }

        if (TextUtils.isEmpty(t3)&&TextUtils.isEmpty(t4)) {

        }else if(TextUtils.isEmpty(t3)){
            ed4.setText(t4);
        }else if(TextUtils.isEmpty(t4)){
            ed3.setText(t3);
        }else{
            ed3.setText(t3);
            ed4.setText(t4);
        }

        if (head==0) {

        }else{
            image.setImageResource(head);
            b2.setVisibility(View.VISIBLE );
        }

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                e1 = ed1.getText().toString().trim();
                e2 = ed2.getText().toString().trim();
                e3 = ed3.getText().toString().trim();
                e4 = ed4.getText().toString().trim();
                int img=head;
                if (TextUtils.isEmpty(e1)) {
                    Toast.makeText(Back_goodsinsert.this, "Please enter item name", Toast.LENGTH_SHORT).show();
                }else if (TextUtils.isEmpty(e2)) {
                    Toast.makeText(Back_goodsinsert.this, "Please enter item type", Toast.LENGTH_SHORT).show();
                }else  if (TextUtils.isEmpty(e3)) {
                    Toast.makeText(Back_goodsinsert.this, "Please enter item price", Toast.LENGTH_SHORT).show();
                }else  if (TextUtils.isEmpty(e4)) {
                    Toast.makeText(Back_goodsinsert.this, "Please enter item description", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(Back_goodsinsert.this, "Published successfully", Toast.LENGTH_SHORT).show();
                    ContentValues values = new ContentValues();

                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date curDate = new Date(System.currentTimeMillis());//获取当前时间
                    String str = formatter.format(curDate);

                    DatabaseMydapter database=new DatabaseMydapter();
                    database.onInsertGoods(img+"",e1,e2,e3,e4);

                    Intent intent = new Intent();
                    finish();
                    intent.putExtra("id",2);//其中1表示Fragment的编号，从0开始编，secondFra的编号为1
                    intent.setClass(Back_goodsinsert.this,MainActivity2.class);
                    startActivity(intent);

                }
            }
        });

    }
}

