package com.example.yuanann.vegetable_app;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class Back_goodsupdate extends AppCompatActivity {
    private static final String TAG = "DBUtils";
    Button b1,b2;
    ImageView bb2;
    EditText ed1,ed2,ed3,ed4;
    String e1,e2,e3,e4;
    String a="null";
    //调用系统相册-选择图片
    private static final int IMAGE = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_back_goodsupdate);

        Intent intent = getIntent();
        //得到所点开的话题的id值
        final int id = intent.getIntExtra("id", 0);
        final int head = intent.getIntExtra("head",0);
        final String t1 = intent.getStringExtra("t1");
        final String t2 = intent.getStringExtra("t2");
        final String t3 = intent.getStringExtra("t3");
        final String t4 = intent.getStringExtra("t4");
        final String myid = id + "";

        ImageView back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back_goodsupdate.this.finish();
            }
        });
        b1 = (Button) findViewById(R.id.bu1);
        b2 = (Button) findViewById(R.id.bu2);
        ed1 = (EditText) findViewById(R.id.ed1);
        ed2 = (EditText) findViewById(R.id.ed2);
        ed3 = (EditText) findViewById(R.id.ed3);
        ed4 = (EditText) findViewById(R.id.ed4);
        bb2 = (ImageView) findViewById(R.id.ziliao);



        @SuppressLint("HandlerLeak") final Handler handler2 = new Handler() {
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                List<HashMap<String, Object>> mlist = (List<HashMap<String, Object>>) msg.obj;
                Iterator<HashMap<String, Object>> its = mlist.iterator();

                String id = null;
                String name = null;
                String type = null;
                String photo = null;
                String price = null;
                String lei = null;
                int i = 0;

                while (its.hasNext()) {
                    HashMap<String, Object> map = new HashMap<String, Object>();
                    map = its.next();
                    name = (String) map.get("name");
                    id = (String) map.get("id");
                    type = (String) map.get("type");
                    photo = (String) map.get("photo");
                    price = (String) map.get("price");
                    lei = (String) map.get("lei");
                    i++;
                }

                ed1.setText(name);
                ed2.setText(lei);
                ed3.setText(price);
                ed4.setText(type);
                if (head==0) {
                    bb2.setImageResource(Integer.parseInt(photo));
                }


                if (TextUtils.isEmpty(t1)&&TextUtils.isEmpty(t2)) {

                }else if(TextUtils.isEmpty(t1)){
                    ed2.setText(t2);
                }else if(TextUtils.isEmpty(t2)){
                    ed1.setText(t1);
                }else{
                    ed1.setText(t1);
                    ed2.setText(t2);
                }

                if (TextUtils.isEmpty(t3)&&TextUtils.isEmpty(t4)) {

                }else if(TextUtils.isEmpty(t3)){
                    ed4.setText(t4);
                }else if(TextUtils.isEmpty(t4)){
                    ed3.setText(t3);
                }else{
                    ed3.setText(t3);
                    ed4.setText(t4);
                }

                if (head==0) {

                }else{
                    bb2.setImageResource(head);
                }
            }
        };
        Thread thread2 = new Thread(new Runnable() {
            List<HashMap<String, Object>> list1 = new ArrayList<HashMap<String, Object>>();

            @Override
            public void run() {
                Mydatabases.getConnection();
                try {
                    list1 = Mydatabases.getgoodsall(id);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Message message = Message.obtain();
                message.what = 1;
                message.obj = list1;
                handler2.sendMessage(message);
            }
        });
        thread2.start();

        bb2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back_goodsupdate.this.finish();
                String t1 = ed1.getText().toString();
                String t2 = ed2.getText().toString();
                String t3= ed3.getText().toString();
                String t4 = ed4.getText().toString();
                finish();
                Intent intent = new Intent();
                intent.setClass(Back_goodsupdate.this,Fm.class);
                intent.putExtra("t1",t1);
                intent.putExtra("t2",t2);
                intent.putExtra("t3",t3);
                intent.putExtra("t4",t4);
                intent.putExtra("id",id);
                intent.putExtra("trans","Item update");
                startActivity(intent);
            }
        });

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                e1 = ed1.getText().toString().trim();
                e2 = ed2.getText().toString().trim();
                e3 = ed3.getText().toString().trim();
                e4 = ed4.getText().toString().trim();

                int img=head;
                if (TextUtils.isEmpty(e1)) {
                    Toast.makeText(Back_goodsupdate.this, "Please enter item name", Toast.LENGTH_SHORT).show();
                }else if (TextUtils.isEmpty(e2)) {
                    Toast.makeText(Back_goodsupdate.this, "Please enter item type", Toast.LENGTH_SHORT).show();
                }else  if (TextUtils.isEmpty(e3)) {
                    Toast.makeText(Back_goodsupdate.this, "Please enter item price", Toast.LENGTH_SHORT).show();
                }else  if (TextUtils.isEmpty(e4)) {
                    Toast.makeText(Back_goodsupdate.this, "Please enter item description", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(Back_goodsupdate.this, "update successful", Toast.LENGTH_SHORT).show();
                    ContentValues values = new ContentValues();

                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date curDate = new Date(System.currentTimeMillis());//获取当前时间
                    String str = formatter.format(curDate);

                    DatabaseMydapter database=new DatabaseMydapter();
                    if (img == 0) {
                        database.onUpdateGoods2(myid,e1,e2,e3,e4);
                    }else{
                        database.onUpdateGoods(myid,img+"",e1,e2,e3,e4);
                    }

                    Intent intent = new Intent();
                    finish();
                    intent.putExtra("id",2);//其中1表示Fragment的编号，从0开始编，secondFra的编号为1
                    intent.setClass(Back_goodsupdate.this,MainActivity2.class);
                    startActivity(intent);
                }
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(Back_goodsupdate.this, "delete successful", Toast.LENGTH_SHORT).show();
                DatabaseMydapter database=new DatabaseMydapter();
                database.onDeletetGoods(myid);
                finish();
                Intent intent = new Intent();
                intent.putExtra("id",2);//其中1表示Fragment的编号，从0开始编，secondFra的编号为1
                intent.setClass(Back_goodsupdate.this, MainActivity2.class);
                startActivity(intent);
            }
        });
    }
    public static boolean isNumeric(String str){
        for (int i = str.length();--i>=0;){
            if (!Character.isDigit(str.charAt(i))){
                return false;
            }
        }
        return true;
    }
}
