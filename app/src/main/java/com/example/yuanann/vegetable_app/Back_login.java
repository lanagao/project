package com.example.yuanann.vegetable_app;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class Back_login extends AppCompatActivity {
    private static final String TAG = "DBUtils";
    Button b1;
    EditText e1,e2;
    String n,p;
    TextView user;
    String aa = "n";
    String st = "y";
    String ps = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //设置当前页面的布局初始化组件
        setContentView(R.layout.activity_back_login);
        b1 = (Button) findViewById(R.id.b1);
        e1 = (EditText) findViewById(R.id.edit1);
        e2 = (EditText) findViewById(R.id.edit2);
        user = (TextView) findViewById(R.id.user);


        user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(Back_login.this, Login.class);
                startActivity(intent);
            }
        });

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String n = e1.getText().toString();
                String pas = e2.getText().toString();
                @SuppressLint("HandlerLeak") final Handler handler2 = new Handler() {
                    @Override
                    public void handleMessage(@NonNull Message msg) {
                        super.handleMessage(msg);
                        final ListView listView = Back_login.this.findViewById(R.id.list);
                        List<HashMap<String, Object>> mlist = (List<HashMap<String, Object>>) msg.obj;
                        Iterator<HashMap<String, Object>> its = mlist.iterator();
                        String id = null;
                        String psd = null;
                        final String[] descA = new String[mlist.size()];
                        final String[] descB = new String[mlist.size()];
                        int p = 0;
                        while (its.hasNext()) {
                            HashMap<String, Object> map = new HashMap<String, Object>();
                            map = its.next();
                            id = (String) map.get("id");
                            descA[p] = id;
                            psd = (String) map.get("psd");
                            descB[p] = psd;
                            p++;
                        }
                        if (p == 0) {
                        } else {
                            aa = "y";
                            id = descA[0];
                            ps = descB[0];
                        }
                        if (TextUtils.isEmpty(n)) {
                            Toast.makeText(getApplicationContext(), "Please enter username", Toast.LENGTH_SHORT).show();
                        } else if (TextUtils.isEmpty(pas)) {
                            Toast.makeText(getApplicationContext(), "Please enter password", Toast.LENGTH_SHORT).show();
                        } else if (aa.equals("n")) {
                            Toast.makeText(getApplicationContext(), "Username doesn't exist", Toast.LENGTH_SHORT).show();
                        } else if (pas.equals(ps)) {
                            Toast.makeText(getApplicationContext(), "Login successful", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent();
                            SharedPreferences sp = getSharedPreferences("Admin", MODE_PRIVATE);
                            SharedPreferences.Editor editor = sp.edit();
                            editor.putString("name", n);
                            editor.putString("adminid", id);
                            editor.putString("psd", pas);
                            editor.commit();
                            intent.setClass(Back_login.this, MainActivity2.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), "Login failed, wrong password" + st, Toast.LENGTH_SHORT).show();
                            e1.setText("");
                            e2.setText("");
                        }
                    }
                };
                Thread thread2 = new Thread(new Runnable() {
                    List<HashMap<String, Object>> list1 = new ArrayList<HashMap<String, Object>>();

                    @Override
                    public void run() {
                        Mydatabases.getConnection();
                        try {
                            list1 = Mydatabases.getAdminname(n);
                            Log.d(TAG, list1.toString());
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        Message message = Message.obtain();
                        message.what = 1;
                        message.obj = list1;
                        handler2.sendMessage(message);
                    }
                });
                thread2.start();
            }
        });
    }
}