package com.example.yuanann.vegetable_app;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class Back_userinsert extends AppCompatActivity {
    private static final String TAG = "DBUtils";
    Button b1;
    EditText ed1,ed2,ed3;
    RadioGroup ed4;
    String state="Login allowed",s="y";
    String u=null;
    String i=null;
    String a="n";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_back_userinsert);
        ImageView back=(ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back_userinsert.this.finish();
            }
        });
        b1 = (Button) findViewById(R.id.bu1);
        ed1 = (EditText) findViewById(R.id.ed1);
        ed2 = (EditText) findViewById(R.id.ed2);
        ed3 = (EditText) findViewById(R.id.ed3);
        ed4 = (RadioGroup) findViewById(R.id.ed4);
        //第一种获得单选按钮值的方法
        //为radioGroup设置一个监听器:setOnCheckedChanged()
        ed4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radbtn = (RadioButton) findViewById(checkedId);
                state="" + radbtn.getText();
                Toast.makeText(getApplicationContext(), state, Toast.LENGTH_SHORT).show();
            }
        });



        b1.setOnClickListener(new View.OnClickListener() {
            String i;
            @Override
            public void onClick(View view) {
                String u= ed1.getText().toString();
                String pss = ed2.getText().toString();
                String add = ed3.getText().toString();
                @SuppressLint("HandlerLeak")
                final Handler handler2 =new Handler(){
                    @Override
                    public void handleMessage(@NonNull Message msg) {
                        super.handleMessage(msg);
                        final ListView listView=Back_userinsert.this.findViewById(R.id.list);
                        List<HashMap<String,Object>> mlist= (List<HashMap<String, Object>>) msg.obj;
                        Iterator<HashMap<String, Object>> its = mlist.iterator();
                        String id =null;
                        final String[] descD = new String[mlist.size()];
                        int p = 0;
                        a="n";
                        while(its.hasNext()){
                            HashMap<String, Object> map = new HashMap<String, Object>();
                            map=its.next();
                            id= (String) map.get("id");
                            descD[p] =id;
                            p++;
                        }
                        if(p==0){

                        }else{
                            a="y";
                        }
                        if (TextUtils.isEmpty(u)) {
                            Toast.makeText(getApplicationContext(), "Please enter username", Toast.LENGTH_SHORT).show();
                        }
                        else if (TextUtils.isEmpty(pss)) {
                            Toast.makeText(getApplicationContext(), "Please enter password", Toast.LENGTH_SHORT).show();
                        }
                        else if (TextUtils.isEmpty(add)) {
                            Toast.makeText(getApplicationContext(), "Please enter user's address", Toast.LENGTH_SHORT).show();
                        }
                        else if (a.equals("y")) {
                            Toast.makeText(getApplicationContext(), "This username has already existed", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            if (TextUtils.isEmpty(i)){
                                i="This guy is lazy~doesn't have anything to say";
                            }
                            Toast.makeText(getApplicationContext(), "Add successful", Toast.LENGTH_SHORT).show();

                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ");
                            Date curDate = new Date(System.currentTimeMillis());//获取当前时间
                            String str = formatter.format(curDate);
                            DatabaseMydapter database=new DatabaseMydapter();
                            database.onInsertUser2(u,pss,add,s,str);

                            Intent intent = new Intent();
                            intent.setClass(Back_userinsert.this, MainActivity2.class);
                            startActivity(intent);
                        }

                    }
                };
                Thread thread2=new Thread(new Runnable() {
                    List<HashMap<String,Object>> list1=new ArrayList<HashMap<String, Object>>();
                    @Override
                    public void run() {
                        Mydatabases.getConnection();
                        try {
                            list1=Mydatabases.geUsername(u);
                            Log.d(TAG, list1.toString());
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        Message message=Message.obtain();
                        message.what=1;
                        message.obj=list1;
                        handler2.sendMessage(message);
                    }
                });
                thread2.start();

            } });

    }
    public static boolean isNumeric(String str){
        for (int i = str.length();--i>=0;){
            if (!Character.isDigit(str.charAt(i))){
                return false;
            }
        }
        return true;
    }
}
