package com.example.yuanann.vegetable_app;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class Back_userupdate extends AppCompatActivity {
    private static final String TAG = "DBUtils";
    Button b1,b2;
    EditText ed1,ed2,ed3;
    RadioGroup ed4;
    String state="可登录";
    String s="y";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_back_userupdate);
        ImageView back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back_userupdate.this.finish();
            }
        });
        b1 = (Button) findViewById(R.id.bu1);
        b2 = (Button) findViewById(R.id.bu2);
        ed1 = (EditText) findViewById(R.id.ed1);
        ed2 = (EditText) findViewById(R.id.ed2);
        ed3 = (EditText) findViewById(R.id.ed3);
        ed4 = (RadioGroup) findViewById(R.id.ed4);

        Intent intent = getIntent();
        //得到所点开的话题的id值
        final int id = intent.getIntExtra("id", 0);
        final String myid = id + "";

        @SuppressLint("HandlerLeak") final Handler handler2 = new Handler() {
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                List<HashMap<String, Object>> mlist = (List<HashMap<String, Object>>) msg.obj;
                Iterator<HashMap<String, Object>> its = mlist.iterator();

                String name = null;
                String psd = null;
                String add = null;
                int i = 0;

                while (its.hasNext()) {
                    HashMap<String, Object> map = new HashMap<String, Object>();
                    map = its.next();
                    name = (String) map.get("name");
                    psd = (String) map.get("psd");
                    add = (String) map.get("add");
                    i++;
                }
                ed1.setText(name);
                ed2.setText(psd);
                ed3.setText(add);
            }
        };
        Thread thread2 = new Thread(new Runnable() {
            List<HashMap<String, Object>> list1 = new ArrayList<HashMap<String, Object>>();

            @Override
            public void run() {
                Mydatabases.getConnection();
                try {
                    list1 = Mydatabases.geUserId(myid);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Message message = Message.obtain();
                message.what = 1;
                message.obj = list1;
                handler2.sendMessage(message);
            }
        });
        thread2.start();


        //第一种获得单选按钮值的方法
        //为radioGroup设置一个监听器:setOnCheckedChanged()
        ed4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radbtn = (RadioButton) findViewById(checkedId);
                state = "" + radbtn.getText();
                if(state.equals("Login allowed")){
                    s="y";
                }else {
                    s="n";
                }
            }
        });

        b1.setOnClickListener(new View.OnClickListener() {
            String i;
            String bbb1 = null;
            String bbb2 = null;
            String bbb3 = null;
            int ww = 0;
            int ll = 0;
            int bb1 = 0;
            int bb2 = 0;
            int bb3 = 0;


            @Override
            public void onClick(View view) {
                String u = ed1.getText().toString();
                String pss = ed2.getText().toString();
                String w = ed3.getText().toString();
                        if (TextUtils.isEmpty(w)) {
                            Toast.makeText(getApplicationContext(), "Please enter user's address", Toast.LENGTH_SHORT).show();
                        } else if (TextUtils.isEmpty(u)) {
                            Toast.makeText(getApplicationContext(), "Please enter username", Toast.LENGTH_SHORT).show();
                        } else if (TextUtils.isEmpty(pss)) {
                            Toast.makeText(getApplicationContext(), "Please enter password", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "update successful", Toast.LENGTH_SHORT).show();

                            DatabaseMydapter database = new DatabaseMydapter();
                            database.updateUser(myid, pss, w, s);

                            Intent intent = new Intent();
                            intent.setClass(Back_userupdate.this, MainActivity2.class);
                            startActivity(intent);
                        }
            }
        });

        ed1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(Back_userupdate.this, "Username cannot be changed", Toast.LENGTH_SHORT).show();
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(Back_userupdate.this, "delete successful", Toast.LENGTH_SHORT).show();
                DatabaseMydapter database=new DatabaseMydapter();
                database.onDeletetUser(myid);

                Intent intent = new Intent();
                intent.setClass(Back_userupdate.this, MainActivity2.class);
                startActivity(intent);
            }
        });


    }
}
