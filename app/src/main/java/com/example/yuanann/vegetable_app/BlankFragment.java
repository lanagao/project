package com.example.yuanann.vegetable_app;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class BlankFragment extends Fragment {


    public BlankFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_blank, container, false);

    }
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        super.onCreate(savedInstanceState);
        Button bu = (Button) getActivity().findViewById(R.id.bu1);

        @SuppressLint("HandlerLeak") final Handler handler = new Handler() {
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                ListView listView = (ListView) getActivity().findViewById(R.id.list);
                List<HashMap<String, Object>> mlist = (List<HashMap<String, Object>>) msg.obj;
                Iterator<HashMap<String, Object>> its = mlist.iterator();
                String id = null;
                final String[] descC = new String[mlist.size()];
                int i = 0;
                while (its.hasNext()) {
                    HashMap<String, Object> map = new HashMap<String, Object>();
                    map = its.next();
                    id = (String) map.get("id");
                    descC[i] = id;
                    i++;
                }

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        int cl = Integer.parseInt((descC[i]));
                        Intent intent = new Intent(getContext(), Back_userupdate.class);
                        intent.putExtra("id", cl);
                        startActivity(intent);
                    }
                });
                if (msg.what == 1) {
                    List<HashMap<String, Object>> list2 = (List<HashMap<String, Object>>) msg.obj;
                    SimpleAdapter simpleAdapter = new SimpleAdapter(getContext(), list2, R.layout.back_user, new String[]{"id","name","psd","state","add","time"},new int[]{R.id.u1,R.id.u2,R.id.u3,R.id.u4,R.id.u5,R.id.u6});
                    listView.setAdapter(simpleAdapter);
                }
            }
        };
        Thread thread = new Thread(new Runnable() {
            List<HashMap<String, Object>> list1 = new ArrayList<HashMap<String, Object>>();

            @Override
            public void run() {
                Mydatabases.getConnection();
                try {

                    list1 = Mydatabases.geUsername();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Message message = Message.obtain();
                message.what = 1;
                message.obj = list1;
                handler.sendMessage(message);
            }
        });
        thread.start();


        bu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getContext(),Back_userinsert.class);
                startActivity(intent);
            }
        });
    }
}
