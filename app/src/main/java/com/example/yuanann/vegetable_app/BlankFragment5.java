package com.example.yuanann.vegetable_app;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 */
public class BlankFragment5 extends Fragment {


    public BlankFragment5() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_blank_fragment5, container, false);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        Button b1;
        EditText ed1,ed2,ed3,ed4;;
        super.onActivityCreated(savedInstanceState);
        super.onCreate(savedInstanceState);
        b1 = (Button)getActivity(). findViewById(R.id.bu1);
        ed1 = (EditText) getActivity(). findViewById(R.id.ed1);
        ed2 = (EditText) getActivity(). findViewById(R.id.ed2);

        SharedPreferences sp =getContext().getSharedPreferences("Admin", MODE_PRIVATE);
        final String name =sp.getString("name", null);
        final String mypsd =sp.getString("psd", null);

        ed1.setText(mypsd);
        ed2.setText(mypsd);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String p= ed1.getText().toString().trim();
                String pa= ed2.getText().toString().trim();
                int l1=p.length();
                int l2=pa.length();
                if (TextUtils.isEmpty(p)) {
                    Toast.makeText(getContext(), "Please enter password", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(pa)) {
                    Toast.makeText(getContext(), "Please enter password again", Toast.LENGTH_SHORT).show();
                } else if (!p.equals(pa)) {
                    Toast.makeText(getContext(), "The password entered twice is different", Toast.LENGTH_SHORT).show();
                } else if (l1>20) {
                    Toast.makeText(getContext(), "Password is set too long", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getContext(), "Password reset complete", Toast.LENGTH_SHORT).show();

                    DatabaseMydapter database=new DatabaseMydapter();
                    database.updateAdmin(name,p);
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("psd", p);
                    editor.commit();

                    Intent intent = new Intent();
                    intent.putExtra("id",1);//其中1表示Fragment的编号，从0开始编，secondFra的编号为1
                    intent.setClass(getContext(),MainActivity2.class);
                    startActivity(intent);

                }
            }

        });

    }
}
