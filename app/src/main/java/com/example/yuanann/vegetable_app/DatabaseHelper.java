package com.example.yuanann.vegetable_app;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DB_NAME="vegetable";//定义数据库名
    private static final String TABLE_NAME="tb_flower";//定义表名
    private static final String CREATE_TABLE="create table "+TABLE_NAME+"( _id integer primary key autoincrement,fname text,type text,info text,user text,date text)";//创建表
    private static final String TABLE_NAME2="myorder";//定义表名
    private static final String CREATE_TABLE2="create table "+TABLE_NAME2+"(_id integer primary key autoincrement,g_id text,g_photo text,g_name text,g_type text,g_price text,g_num text,g_check text,user,date text)";//创建表
    private static final String TABLE_NAME3="mygoods";//定义表名
    private static final String CREATE_TABLE3="create table "+TABLE_NAME3+"(_id integer primary key autoincrement,g_photo text,g_name text,g_type text,g_lei text,g_price text)";//创建表
    private static final String TABLE_NAME4="cart";//定义表名
    private static final String CREATE_TABLE4="create table "+TABLE_NAME4+"(cart_id integer primary key autoincrement,g_id text,g_photo text,g_name text,g_type text,g_price text,g_num text,g_check text,user text,date text)";//创建表
    private static final String TABLE_NAME5="waitorder";//定义表名
    private static final String CREATE_TABLE5="create table "+TABLE_NAME5+"(_id integer primary key autoincrement,g_id text,g_photo text,g_name text,g_type text,g_price text,g_num text,g_check text,user text,date text)";//创建表
    private static final String TABLE_NAME6="user";//定义表名
    private static final String CREATE_TABLE6="create table "+TABLE_NAME6+"(_id integer primary key autoincrement,u_name text,u_psd text,u_type text,u_sex text,u_tel text,u_photo text,u_time text)";//创建表
    private static final String TABLE_NAME7="userfouce";//定义表名
    private static final String CREATE_TABLE7="create table "+TABLE_NAME7+"(_id integer primary key autoincrement,f_name text,f_type text,f_photo text,user text,date text)";//创建表
    private static final String TABLE_NAME8="admin";//定义表名
    private static final String CREATE_TABLE8="create table "+TABLE_NAME8+"(_id integer primary key autoincrement,admin_name text,admin_psd text,admin_time text)";//创建表
//    private static final String TABLE_NAME9="cart";//定义表名
//    private static final String CREATE_TABLE9="CREATE TABLE"+TABLE_NAME9+"(cart_id INTEGER PRIMARY KEY AUTOINCREMENT,g_id,g_photo,g_name,g_type,g_price,g_num,g_check,user,date)";
    //创建SQLiteDatabase实例
    private SQLiteDatabase db;
    //构造方法
    public DatabaseHelper(Context context){
        super(context,DB_NAME,null,1);
    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //实例化SQLiteDatabase
        db=sqLiteDatabase;
        //创建表
        db.execSQL(CREATE_TABLE);
        db.execSQL(CREATE_TABLE2);
        db.execSQL(CREATE_TABLE3);
        db.execSQL(CREATE_TABLE4);
        db.execSQL(CREATE_TABLE5);
        db.execSQL(CREATE_TABLE6);
        db.execSQL(CREATE_TABLE7);
        db.execSQL(CREATE_TABLE8);
//        db.execSQL(CREATE_TABLE9);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        db=sqLiteDatabase;
        //创建表
        db.execSQL("DROP TABLE IF EXISTS tb_flower");
        db.execSQL("DROP TABLE IF EXISTS myorder");
        db.execSQL("DROP TABLE IF EXISTS mygoods");
        db.execSQL("DROP TABLE IF EXISTS cart");
        db.execSQL("DROP TABLE IF EXISTS waitorder");
        db.execSQL("DROP TABLE IF EXISTS user");
        db.execSQL("DROP TABLE IF EXISTS userfouce");
        db.execSQL("DROP TABLE IF EXISTS admin");
        db.execSQL("DROP TABLE IF EXISTS cart");
        onCreate(db);
    }
    //添加数据
    public void insert(ContentValues values){
        //将数据库设置为可读写的
        db=getWritableDatabase();
        db.insert(TABLE_NAME,null,values);
        db.close();
    }
    //添加用户数据
    public void insertUser(ContentValues values){
        //将数据库设置为可读写的
        db=getWritableDatabase();
        db.insert(TABLE_NAME6,null,values);
        db.close();
    }
    //添加用户关注数据
    public void insertUserfouce(ContentValues values){
        //将数据库设置为可读写的
        db=getWritableDatabase();
        db.insert(TABLE_NAME7,null,values);
        db.close();
    }
    //添加订单数据
    public void insertVege(ContentValues values){
        //将数据库设置为可读写的
        db=getWritableDatabase();
        db.insert(TABLE_NAME2,null,values);
        db.close();
    }
    //添加待支付订单数据
    public void insertWaitvege(ContentValues values){
        //将数据库设置为可读写的
        db=getWritableDatabase();
        db.insert(TABLE_NAME5,null,values);
        db.close();
    }
    //添加待支付订单数据
    public void insertCart(ContentValues values){
        //将数据库设置为可读写的
        db=getWritableDatabase();
        db.insert(TABLE_NAME8,null,values);
        db.close();
    }
    //添加数据
    public void insertAll(ContentValues values){
        //将数据库设置为可读写的
        db=getWritableDatabase();
        db.insert(TABLE_NAME3,null,values);
        db.close();
    }
    //删除数据
    public void del(int id){
        db=getWritableDatabase();
        db.delete(TABLE_NAME,"_id=?",new String[]{String.valueOf(id)});
        db.close();
    }
    //查询数据
    public Cursor query(){
        db=getWritableDatabase();
        Cursor cursor=db.query(TABLE_NAME,null,null,null,null,null,"_id desc");
        //db.close();
        return cursor;
    }
    //查询数据
    public Cursor queryCart(){
        db=getWritableDatabase();
        Cursor cursor=db.query(TABLE_NAME8,null,null,null,null,null,"_id desc");
        //db.close();
        return cursor;
    }

    //根据名称查询对应加入的课程
    public Cursor querymygoods(int id){
        db=getWritableDatabase();
        Cursor cursor=db.query(TABLE_NAME3,null,"_id like ?", new String[] { id+"" },null,null,"_id asc");
        //db.close();
        return cursor;
    }

    //查询数据
    public Cursor queryMygoods(){
        db=getWritableDatabase();
        Cursor cursor=db.query(TABLE_NAME3,null,null,null,null,null,"_id asc");
        //db.close();
        return cursor;
    }

    //查询商品
    public Cursor queryMygoods_Select(){
        db=getWritableDatabase();
        Cursor cursor=db.query(TABLE_NAME3,null,"_id <= 8",null,null,null,"_id asc");
        //db.close();
        return cursor;
    }

    //查询商品
    public Cursor queryMygoods_Select2(){
        db=getWritableDatabase();
        Cursor cursor=db.query(TABLE_NAME3,null,"_id > 8 and _id <= 16",null,null,null,"_id asc");
        //db.close();
        return cursor;
    }

    //查询商品
    public Cursor queryMygoods_Select3(){
        db=getWritableDatabase();
        Cursor cursor=db.query(TABLE_NAME3,null,"_id >= 42 and _id < 50",null,null,null,"_id asc");
        //db.close();
        return cursor;
    }

    //查询商品
    public Cursor queryMygoods_Select4(){
        db=getWritableDatabase();
        Cursor cursor=db.query(TABLE_NAME3,null,"_id > 16 and _id <= 24",null,null,null,"_id asc");
        //db.close();
        return cursor;
    }

    //查询商品
    public Cursor queryMygoods_type1(){
        db=getWritableDatabase();
        Cursor cursor=db.query(TABLE_NAME3,null,"_id > 4 and _id <= 12",null,null,null,"_id asc");
        //db.close();
        return cursor;
    }

    //查询商品
    public Cursor queryMygoods_type4(){
        db=getWritableDatabase();
        Cursor cursor=db.query(TABLE_NAME3,null,"_id >= 42 and _id <= 50",null,null,null,"_id asc");
        //db.close();
        return cursor;
    }

    //查询商品
    public Cursor queryMygoods_type5(){
        db=getWritableDatabase();
        Cursor cursor=db.query(TABLE_NAME3,null,"_id > 50 and _id <= 58",null,null,null,"_id asc");
        //db.close();
        return cursor;
    }

    //删除商品
    public void delgoods(String name,int id){
        db=getWritableDatabase();
        String sql = "delete from "+TABLE_NAME4+" where user='"+name+"' and g_id="+id;
        db.execSQL(sql);
        db.close();
    }

    //查询商品
    public Cursor queryMygoods_type6(){
        db=getWritableDatabase();
        Cursor cursor=db.query(TABLE_NAME3,null,"_id > 58 and _id <= 66",null,null,null,"_id asc");
        //db.close();
        return cursor;
    }

    //查询商品
    public Cursor queryMygoods_type10(){
        db=getWritableDatabase();
        Cursor cursor=db.query(TABLE_NAME3,null,"_id > 66 and _id <= 74",null,null,null,"_id asc");
        //db.close();
        return cursor;
    }

    //查询商品
    public Cursor queryMygoods_type7(){
        db=getWritableDatabase();
        Cursor cursor=db.query(TABLE_NAME3,null,"_id > 64 and _id <= 72",null,null,null,"_id asc");
        //db.close();
        return cursor;
    }

    //查询商品
    public Cursor queryMygoods_type2(){
        db=getWritableDatabase();
        Cursor cursor=db.query(TABLE_NAME3,null,"_id > 12 and _id <= 20",null,null,null,"_id asc");
        //db.close();
        return cursor;
    }

    //查询商品
    public Cursor queryMygoods_type3(){
        db=getWritableDatabase();
        Cursor cursor=db.query(TABLE_NAME3,null,"_id > 16 and _id <= 24",null,null,null,"_id asc");
        //db.close();
        return cursor;
    }

    //查询商品
    public Cursor queryMygoods_type8(){
        db=getWritableDatabase();
        Cursor cursor=db.query(TABLE_NAME3,null,"_id > 24 and _id <= 32",null,null,null,"_id asc");
        //db.close();
        return cursor;
    }

    //查询商品
    public Cursor queryMygoods_type9(){
        db=getWritableDatabase();
        Cursor cursor=db.query(TABLE_NAME3,null,"_id > 32 and _id <= 49",null,null,null,"_id asc");
        //db.close();
        return cursor;
    }

    //根据id查询数据
    public Cursor queryid(String mykey){
        db=getWritableDatabase();
        Cursor cursor=db.query(TABLE_NAME3,null,"g_name like ?", new String[] { "%"+mykey+"%" },null,null,"_id desc");
        //db.close();
        return cursor;
    }

    //查询订单的数据
    public Cursor queryorder(){
        db=getWritableDatabase();
        Cursor cursor=db.query(TABLE_NAME2,null,null,null,null,null,"_id desc");
        //db.close();
        return cursor;
    }

    //查询待支付订单的数据
    public Cursor querywaitorder(){
        db=getWritableDatabase();
        Cursor cursor=db.query(TABLE_NAME5,null,null,null,null,null,"_id desc");
        //db.close();
        return cursor;
    }

    //查询待支付订单指定id
    public Cursor queryidwaitorder(){
        db=getWritableDatabase();
        Cursor cursor=db.query(TABLE_NAME5,null,null,null,null,null,"_id desc");
        //db.close();
        return cursor;

    }

    //查询关注总数
    public Cursor allfouce( ){
        db=getWritableDatabase();
        Cursor cursor = db.query(TABLE_NAME7,null,null,null,null,null,"_id desc");
        return cursor;
    }

    public long allCaseNum( ){
        db=getWritableDatabase();
        Cursor cursor = db.rawQuery("select count(_id) from mygoods",null);
        cursor.moveToFirst();
        int count = cursor.getInt(0);
        cursor.close();
        return count;
    }


}

