package com.example.yuanann.vegetable_app;

import android.database.Cursor;
import android.util.Log;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Yuanann on 2022-4-13.
 */

public class DatabaseMydapter {
    private static final String REMOTE_IP = "10.0.2.2:3306";
    private static final String URL = "jdbc:mysql://" + REMOTE_IP + "/myvegetable"+"?useUnicode=true&characterEncoding=UTF-8&useSSL=false";
    private static final String USER = "root";
    private static final String PASSWORD = "abc123";
    private Connection conn;

    protected void onDestroy() {
//        super.onDestroy();
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                conn = null;
            } finally {
                conn = null;
            }
        }
    }

    public void onQueryUser() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                conn = Util.openConnection(URL, USER, PASSWORD);
                Log.i("onConn", "onConn");
                Util.query(conn, "select * from user");
                Log.i("onQuery", "onQuery123");
            }
        }).start();
    }

    public Cursor onQueryUserfouce() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                conn = Util.openConnection(URL, USER, PASSWORD);
                Log.i("onConn", "onConn");
                Util.queryFouce(conn, "select * from userfouce");
                Log.i("onQuery", "onQuery123");
            }
        }).start();
        return null;
    }

    public void onConn() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                onDestroy();
                conn = Util.openConnection(URL, USER, PASSWORD);
                Log.i("onConn", "onConn");
            }
        }).start();
    }

    public void onInsertUser(String name, String psd, String time) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                conn = Util.openConnection(URL, USER, PASSWORD);
                Log.i("onConn", "onConn");
                String sql = "insert into user (u_name,u_psd,u_state,u_time) VALUES ('" + name + "','" + psd + "','y','" + time + "')";
                Util.execSQL(conn, sql);
                Log.i("onInsert", sql);
            }
        }).start();
    }

    public void onInsertUser2(String name, String psd, String add, String state, String time) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                conn = Util.openConnection(URL, USER, PASSWORD);
                Log.i("onConn", "onConn");
                String sql = "insert into user (u_name,u_psd,u_add,u_state,u_time) VALUES ('" + name + "','" + psd + "','" + add + "','" + state + "','" + time + "')";
                Util.execSQL(conn, sql);
                Log.i("onInsert", sql);
            }
        }).start();
    }

    public void onInsertGoods(String photo, String name, String lei, String price, String type) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                conn = Util.openConnection(URL, USER, PASSWORD);
                Log.i("onConn", "onConn");
                String sql = "insert into mygoods (g_photo,g_name,g_type,g_price,g_lei,g_img) VALUES ('" + photo + "','" + name + "','" + type + "','" + price + "','" + lei + "','" + photo + "')";
                Util.execSQL(conn, sql);
                Log.i("onInsert", sql);
            }
        }).start();
    }

    public void onInsertOrder(String id, String photo, String name, String type, String price, String num, String check, String user, String date, String add) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                conn = Util.openConnection(URL, USER, PASSWORD);
                Log.i("onConn", "onConn");
                String sql = "insert into cart (g_id,g_photo,g_name,g_type,g_price,g_num,g_check,user,date,g_address) VALUES ('" + id + "','" + photo + "','" + name + "','" + type + "','" + price + "','" + num + "','" + check + "','" + user + "','" + date + "','" + add + "')";
                Util.execSQL(conn, sql);
                Log.i("onInsert", sql);
            }
        }).start();
    }

    public void onInsertWaieOrder(String id, String photo, String name, String type, String price, String num, String check, String user, String date, String add) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                conn = Util.openConnection(URL, USER, PASSWORD);
                Log.i("onConn", "onConn");
                String sql = "insert into waitorder (g_id,g_photo,g_name,g_type,g_price,g_num,g_check,user,date,g_address) VALUES ('" + id + "','" + photo + "','" + name + "','" + type + "','" + price + "','" + num + "','" + check + "','" + user + "','" + date + "','" + add + "')";
                Util.execSQL(conn, sql);
                Log.i("onInsert", sql);
            }
        }).start();
    }

    public void onInsertMyOrder(String id, String photo, String name, String type, String price, String num, String check, String user, String date, String add) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                conn = Util.openConnection(URL, USER, PASSWORD);
                Log.i("onConn", "onConn");
                String sql = "insert into myorder (g_id,g_photo,g_name,g_type,g_price,g_num,g_check,user,date,g_address) VALUES ('" + id + "','" + photo + "','" + name + "','" + type + "','" + price + "','" + num + "','" + check + "','" + user + "','" + date + "','" + add + "')";
                Util.execSQL(conn, sql);
                Log.i("onInsert", sql);
            }
        }).start();
    }

    public void onInsertWaiteOrder(String id, String photo, String name, String type, String price, String num, String check, String user, String date, String add) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                conn = Util.openConnection(URL, USER, PASSWORD);
                Log.i("onConn", "onConn");
                String sql = "insert into waitorder (g_id,g_photo,g_name,g_type,g_price,g_num,g_check,user,date,g_address) VALUES ('" + id + "','" + photo + "','" + name + "','" + type + "','" + price + "','" + num + "','" + check + "','" + user + "','" + date + "','" + add + "')";
                Util.execSQL(conn, sql);
                Log.i("onInsert", sql);
            }
        }).start();
    }

    public void onInsertFouce( String name, String type,int photo,String user, String date) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                conn = Util.openConnection(URL, USER, PASSWORD);
                Log.i("onConn", "onConn");
                String sql = "insert into userfouce (f_name,f_type,f_photo,user,date) VALUES ('" + name + "','" + type + "','" + photo + "','" + user + "','" + date +"')";
                Util.execSQL(conn, sql);
                Log.i("onInsert", sql);
            }
        }).start();
    }

    public void onInsertComment( String name, String type,String info,String user, String date) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                conn = Util.openConnection(URL, USER, PASSWORD);
                Log.i("onConn", "onConn");
                String sql = "insert into tb_flower (fname,type,info,user,date) VALUES ('" + name + "','" + type + "','" + info + "','" + user + "','" + date +"')";
                Util.execSQL(conn, sql);
                Log.i("onInsert", sql);
            }
        }).start();
    }

    public void onDelete() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                conn = Util.openConnection(URL, USER, PASSWORD);
                Log.i("onConn", "onConn");
                String sql = "delete from user where u_name='fff'";
                Util.execSQL(conn, sql);
                Log.i("onDelete", "onDelete");
            }
        }).start();
    }

    public void onDeleteFouce(String id) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                conn = Util.openConnection(URL, USER, PASSWORD);
                Log.i("onConn", "onConn");
                String sql = "delete from userfouce where f_id='"+id+"' order by f_id desc";
                Util.execSQL(conn, sql);
                Log.i("onDelete", "onDelete");
            }
        }).start();
    }

    public void onDeletecart(String name,int id) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                conn = Util.openConnection(URL, USER, PASSWORD);
                Log.i("onConn", "onConn");
                String sql = "delete from cart where user='"+name+"' and g_id="+id;
                Util.execSQL(conn, sql);
                Log.i("onDelete", "onDelete");
            }
        }).start();
    }

    public void onDeletecartgoods(String name,String id) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                conn = Util.openConnection(URL, USER, PASSWORD);
                Log.i("onConn", "onConn");
                String sql = "delete from cart where user='"+name+"' and g_id='"+id+"'";
                Util.execSQL(conn, sql);
                Log.i("onDelete", "onDelete");
            }
        }).start();
    }


    public void onUpdateCart(int num, String name, int id) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                conn = Util.openConnection(URL, USER, PASSWORD);
                Log.i("onConn", "onConn");
                String sql = "update cart set g_num='" + num + "' where user='" + name + "' and g_id='" + id + "'";
                Util.execSQL(conn, sql);
                Log.i("onUpdate", "onUpdate");
            }
        }).start();
    }

    public void onUpdateGoods(String id,String photo,String name, String lei, String price, String type) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                conn = Util.openConnection(URL, USER, PASSWORD);
                Log.i("onConn", "onConn");
                String sql = "update mygoods set g_name='" + name + "', g_photo='" + photo + "',g_type='" + type + "',g_price='" + price + "',g_lei='" + lei + "' where g_id='" + id + "'";
                Util.execSQL(conn, sql);
                Log.i("onInsert", sql);
            }
        }).start();
    }

    public void onUpdateGoods2(String id,String name, String lei, String price, String type) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                conn = Util.openConnection(URL, USER, PASSWORD);
                Log.i("onConn", "onConn");
                String sql = "update mygoods set g_name='" + name + "',g_type='" + type + "',g_price='" + price + "',g_lei='" + lei + "' where g_id='" + id + "'";
                Util.execSQL(conn, sql);
                Log.i("onInsert", sql);
            }
        }).start();
    }

    public void onUpdateAdd(String name, String add) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                conn = Util.openConnection(URL, USER, PASSWORD);
                Log.i("onConn", "onConn");
                String sql = "update user set u_add='" + add + "' where u_name='" + name + "'";
                Util.execSQL(conn, sql);
                Log.i("onUpdate", "onUpdate");
            }
        }).start();
    }

    public void onUpdateWaitOrder() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                conn = Util.openConnection(URL, USER, PASSWORD);
                Log.i("onConn", "onConn");
                String sql = "update waitorder set g_check='false' where g_check='true'";
                Util.execSQL(conn, sql);
                Log.i("onUpdate", "onUpdate");
            }
        }).start();
    }

    public void updateUser(final String id, final String psd, final String add, final String state) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                conn = Util.openConnection(URL, USER, PASSWORD);
                Log.i("onConn", "onConn");
                String sql = "update user set u_psd='"+psd+"',u_add='"+add+"',u_state='"+state+"' where u_id='"+id+"'";
                Util.execSQL(conn, sql);
                Log.i("onUpdate", "onUpdate");
            }
        }).start();
    }

    public void onDeletetUser(final String id) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                conn = Util.openConnection(URL, USER, PASSWORD);
                Log.i("onConn", "onConn");
                String sql = "delete from user where u_id='"+id+"'";
                Util.execSQL(conn, sql);
                Log.i("onDelete", "onDelete");
            }
        }).start();
    }

    public void onDeletetGoods(final String id) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                conn = Util.openConnection(URL, USER, PASSWORD);
                Log.i("onConn", "onConn");
                String sql = "delete from mygoods where g_id='"+id+"'";
                Util.execSQL(conn, sql);
                Log.i("onDelete", "onDelete");
            }
        }).start();
    }

    public void onDeletetOrder(final String id) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                conn = Util.openConnection(URL, USER, PASSWORD);
                Log.i("onConn", "onConn");
                String sql = "delete from myorder where o_id='"+id+"'";
                Util.execSQL(conn, sql);
                Log.i("onDelete", "onDelete");
            }
        }).start();
    }

    public void onDeletetComment(final String id) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                conn = Util.openConnection(URL, USER, PASSWORD);
                Log.i("onConn", "onConn");
                String sql = "delete from tb_flower where t_id='"+id+"'";
                Util.execSQL(conn, sql);
                Log.i("onDelete", "onDelete");
            }
        }).start();
    }

    public void updateAdmin(final String name, final String psd) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                conn = Util.openConnection(URL, USER, PASSWORD);
                Log.i("onConn", "onConn");
                String sql = "update admin set a_psd='"+psd+"' where a_name='"+name+"'";
                Util.execSQL(conn, sql);
                Log.i("onUpdate", "onUpdate");
            }
        }).start();
    }


    public void onQuery() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                conn = Util.openConnection(URL, USER, PASSWORD);
                Log.i("onConn", "onConn");
                Util.query(conn, "select * from user");
                Log.i("onQuery", "onQuery");
            }
        }).start();
    }
}