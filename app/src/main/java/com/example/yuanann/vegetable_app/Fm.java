package com.example.yuanann.vegetable_app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Fm extends AppCompatActivity {
    AlertDialog.Builder dialog = null;
    int n2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fm);

        GridView list2=(GridView)findViewById(R.id.grid);
        ImageView back=(ImageView)findViewById(R.id.back);

        Intent intent = getIntent();
        final String trans = intent.getStringExtra("trans");
        final String t1 = intent.getStringExtra("t1");
        final String t2 = intent.getStringExtra("t2");
        final String t3 = intent.getStringExtra("t3");
        final String t4 = intent.getStringExtra("t4");
        final int id = intent.getIntExtra("id",0);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fm.this.finish();
            }
        });

        final int[] imageA={R.mipmap.n1,R.mipmap.n2,R.mipmap.n3,R.mipmap.n4, R.mipmap.n5,R.mipmap.n6,R.mipmap.n7,R.mipmap.n8,R.mipmap.n9, R.mipmap.n10,
                R.mipmap.n11,R.mipmap.n12,R.mipmap.n13,R.mipmap.n14, R.mipmap.n15,R.mipmap.n16,R.mipmap.n17,R.mipmap.n18,R.mipmap.n19, R.mipmap.n20,
                R.mipmap.n21,R.mipmap.n22,R.mipmap.n23,R.mipmap.n24,R.mipmap.n26,R.mipmap.n27,R.mipmap.n28,R.mipmap.n29,R.mipmap.n30, R.mipmap.n31,R.mipmap.n32,R.mipmap.n33,R.mipmap.n34,R.mipmap.n35, R.mipmap.n36,
                R.mipmap.n37,R.mipmap.n38,R.mipmap.n39,R.mipmap.n40, R.mipmap.n41,R.mipmap.n42,R.mipmap.n43,R.mipmap.n44,R.mipmap.n45, R.mipmap.n46,
                R.mipmap.n47,R.mipmap.n48,R.mipmap.n49,R.mipmap.n50,R.mipmap.n51,R.mipmap.n52,R.mipmap.n53,R.mipmap.n54,R.mipmap.n55,R.mipmap.n56,R.mipmap.n57,R.mipmap.n58,
                R.mipmap.n59,R.mipmap.n60,R.mipmap.n61,R.mipmap.n62,R.mipmap.n63,R.mipmap.n64,R.mipmap.n65,R.mipmap.n66,R.mipmap.n67,R.mipmap.n68,R.mipmap.n69,R.mipmap.n70,
                R.mipmap.n71,R.mipmap.n72,R.mipmap.n73,R.mipmap.n74,R.mipmap.n75};

        List<Map<String,Object>> listItems=new ArrayList<Map<String,Object>>();
        for(int i=0;i<imageA.length;i++){
            Map<String,Object> item=new HashMap<String, Object>();
            item.put("a1",imageA[i]);
            listItems.add(item);
        }
        SimpleAdapter simpleAdapter=new SimpleAdapter(Fm.this,listItems,R.layout.layout_fm,new String[]{"a1"},new int[]{R.id.t1});
        //设置适配器
        list2.setAdapter(simpleAdapter);

        list2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final int mynum=imageA[i];
                dialog = new AlertDialog.Builder(Fm.this);
                dialog.setTitle("Tips");
                dialog.setMessage("Are you sure you want to select this image？");
                dialog.setCancelable(false);
                dialog.setPositiveButton("yes",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if(trans.equals("Add Item")){
                            Fm.this.finish();
                            Intent intent=new Intent(Fm.this,Back_goodsinsert.class);
                            intent.putExtra("t1",t1);
                            intent.putExtra("t2",t2);
                            intent.putExtra("t3",t3);
                            intent.putExtra("t4",t4);
                            intent.putExtra("head",mynum);
                            startActivity(intent);
                        }else{
                            Fm.this.finish();
                            Intent intent=new Intent(Fm.this,Back_goodsupdate.class);
                            intent.putExtra("t1",t1);
                            intent.putExtra("t2",t2);
                            intent.putExtra("t3",t3);
                            intent.putExtra("t4",t4);
                            intent.putExtra("id",id);
                            intent.putExtra("head",mynum);
                            startActivity(intent);
                        }
                    }
                });
                dialog.setNegativeButton("cancel",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                    Toast.makeText(MainActivity2.this, "You clicked No",Toast.LENGTH_SHORT).show();
                    }
                });
                dialog.show();
            }
        });

    }
}
