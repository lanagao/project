package com.example.yuanann.vegetable_app;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yuanann.vegetable_app.base.BaseFragment;
import com.example.yuanann.vegetable_app.cart.CartGoods;
import com.example.yuanann.vegetable_app.cart.CartListAdapter;
import com.example.yuanann.vegetable_app.cart.DingdanActivity;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class Goodss extends BaseFragment {

    private ListView cart_list;
    private TextView cart_money, cart_check_all, cart_del, cart_buy;
    private TextView num;
    private DatabaseHelper dbOpenHelper;
    private boolean flag = true;
    private LinearLayout all;
    String myname;
    @Override
    public View initView() {
        View view = View.inflate(mContext, R.layout.activity_goods, null);
        cart_list = view.findViewById(R.id.cart_list);
        all=view.findViewById(R.id.all);
        cart_money = view.findViewById(R.id.cart_money);
        num = view.findViewById(R.id.num);
        cart_check_all = view.findViewById(R.id.cart_check_all);
        cart_del = view.findViewById(R.id.cart_del);
        cart_buy = view.findViewById(R.id.cart_buy);
        initListener();
        cart_money.performClick();
        return view;
    }

    public void initData(){

        ArrayList<CartGoods> cartGoods = listAllCartGoods();
        cart_list.setAdapter(new CartListAdapter(mContext,cartGoods));

        super.initData();
    }

    private void initListener(){
        Handler handler = new Handler() {
            public void handleMessage(android.os.Message msg) {
                if (msg.what == 0 && isAdded()) {
                    double money = 0.0;
                    ArrayList<CartGoods> goods = listAllCartGoods();
                    for (CartGoods g : goods) {
                        if ("true".equals(g.getC_checked())) {
                            money += g.getC_num() * g.getG_price();
                        }
                    }
                    cart_money.setText("总共:￥" + money);
                }
            }
        };

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    handler.sendEmptyMessage(0);
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
        SharedPreferences sp =  getActivity().getSharedPreferences("name", MODE_PRIVATE);
        myname=sp.getString("name", null);
        //全选单击事件
        cart_check_all.setOnClickListener(v -> {
            if (flag){
                dbOpenHelper = new DatabaseHelper(mContext);
                ContentValues values = new ContentValues();
                values.put("g_check", "true");
                //参数:表名，修改后的值，where条件，以及约束，如果不指定三四两个参数，会更改所有行
                dbOpenHelper.getWritableDatabase().update(
                        "cart",
                        values,
                        null,
                        null);
                dbOpenHelper.close();
                flag = false;
            }else {
                dbOpenHelper = new DatabaseHelper(mContext);
                ContentValues values = new ContentValues();
                values.put("g_check", "false");
                //参数:表名，修改后的值，where条件，以及约束，如果不指定三四两个参数，会更改所有行
                dbOpenHelper.getWritableDatabase().update(
                        "cart",
                        values,
                        null,
                        null);
                dbOpenHelper.close();
                flag = true;
            }
            initData();

            double money = 0.0;
            ArrayList<CartGoods> goods = listAllCartGoods();
            for (CartGoods g : goods){
                if ("true".equals(g.getC_checked())){
                    money += g.getC_num() * g.getG_price();
                }
            }
            cart_money.setText("Total:￥" + money);
        });

        //合计单击事件
        cart_money.setOnClickListener(v -> {
            double money = 0.0;
            ArrayList<CartGoods> goods = listAllCartGoods();
            for (CartGoods g : goods){
                if ("true".equals(g.getC_checked())){
                    money += g.getC_num() * g.getG_price();
                }
            }
            cart_money.setText("Total:￥" + money);
        });

        //删除单击事件
        cart_del.setOnClickListener(v -> {
            MyDialog myDialog = new MyDialog(mContext,R.style.MyDialog);
            myDialog.setTitle("Warn")
                    .setMessage("Really want to delete this item？")
                    .setCancel("cancel", dialog -> dialog.dismiss() )
                    .setConfirm("confirm", dialog -> {
                        ArrayList<CartGoods> goods = listAllCartGoods();
                        for (CartGoods g : goods){
                            if ("true".equals(g.getC_checked())){
                                DatabaseHelper dbHelper = new DatabaseHelper(getContext());
                                dbHelper.delgoods(myname,g.getG_id());
                                DatabaseMydapter database = new DatabaseMydapter();
                                database.onDeletecart(myname,g.getG_id());
                            }
                        }
                        initData();
                        Toast.makeText(mContext, "Deleted successfully", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    })
                    .show();
        });

        //去结算单击事件
        cart_buy.setOnClickListener(v -> {
            double money = 0.0;
            boolean isEmp = false;
            ArrayList<CartGoods> goods = listAllCartGoods();
            for (CartGoods g : goods){
                if ("true".equals(g.getC_checked())){
                    isEmp = true;
                }
            }
            if (!isEmp){
                Toast.makeText(mContext, "You haven't chosen any item", Toast.LENGTH_SHORT).show();
            }else {
                for (CartGoods g : goods){
                    if ("true".equals(g.getC_checked())){
                        money += g.getC_num() * g.getG_price();
                    }
                }
                Intent intent = new Intent();
                intent.putExtra("money",money);
                intent.setClass(mContext,DingdanActivity.class);
                startActivity(intent);
            }
        });
    }

    //删除购物车中指定商品
    private void delCartGoods(int g_id,String name){
        dbOpenHelper = new DatabaseHelper(mContext);
        //参数依次是表名，以及where条件与约束
        dbOpenHelper.getWritableDatabase().delete(
                "cart",
                "g_id=?",
                new String[]{String.valueOf(g_id)});
        dbOpenHelper.close();
    }

    //查询数据库中所有商品
    private ArrayList<CartGoods> listAllCartGoods(){
        dbOpenHelper = new DatabaseHelper(mContext);
        Cursor cursor = dbOpenHelper.getReadableDatabase().query(
                "cart",null,"user like ?", new String[] { myname+"" },null,null,null
        );
        ArrayList<CartGoods> cartGoods = new ArrayList<>();
        //cart(cart_id,g_id,g_photo,g_name,g_type,g_price,g_num)
        int a=0;
        while (cursor.moveToNext()){
            a = a + 1;
            int g_id = Integer.parseInt(cursor.getString(1));
            int g_photo = Integer.parseInt(cursor.getString(2));
            String g_name = cursor.getString(3);
            String g_type = cursor.getString(4);
            double g_price = Double.parseDouble(cursor.getString(5));
            int c_num = Integer.parseInt(cursor.getString(6));
            String c_check = cursor.getString(7);
            cartGoods.add(new CartGoods(g_id, g_photo, g_name, g_type, g_price, c_num, c_check));
        }
        if(a==0) {
            all.setBackgroundResource(R.drawable.white);
            cart_list.setBackgroundResource(R.drawable.nogoods1);
        }
        num.setText(a+"items in your cart");
        cursor.close();
        dbOpenHelper.close();
        return cartGoods;
    }

}

