package com.example.yuanann.vegetable_app;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;

import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import pl.droidsonroids.gif.GifImageView;

import static android.content.Context.MODE_PRIVATE;


public class Index extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_index, container, false);
        return view;
    }
    private static final String  TAG="DBUtils";
//    private Handler handler ;
    private int[] imagearr = { R.drawable.a1, R.drawable.a4,
            R.drawable.a5, R.drawable.a6, R.drawable.a7};
    private int count = 0;
//    private ImageView imageview;
    private LinearLayout ba;
    Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (msg.what == 0&&isAdded()) {
//                imageview.setImageResource(imagearr[count++]);
//                ba.setBackgroundResource(imagearr[count++]);

                Drawable drawable = getResources().getDrawable(imagearr[count++]);
                View temp =getActivity().findViewById(R.id.ba);
                temp.setBackgroundDrawable(drawable);
                if (count >= imagearr.length) {
                    count = 0;
                }
            }
        };
    };

    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        super.onCreate(savedInstanceState);
//        imageview = (ImageView) this.getActivity().findViewById(R.id.imageview);
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    handler.sendEmptyMessage(0);

                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();


        final TextView x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,tt1,tt2,tt3,tt4,tt5,tt6,ttt1,ttt2,ttt3,ttt4,ttt5,ttt6;
        final ImageView t1,t2,t3,t4,t5,t6,tj1,tj2,zc1,zc2,zc3,zc4;
        final Button cd,cq,xa,sh;
        final LinearLayout ba;
        final EditText searchbutton;
        final GifImageView yhq;
        x1= (TextView) getActivity().findViewById(R.id.x1);
        x2= (TextView) getActivity().findViewById(R.id.x2);
        x3= (TextView) getActivity().findViewById(R.id.x3);
        x4= (TextView) getActivity().findViewById(R.id.x4);
        x5= (TextView) getActivity().findViewById(R.id.x5);
        x6= (TextView) getActivity().findViewById(R.id.x6);
        x7= (TextView) getActivity().findViewById(R.id.x7);
        x8= (TextView) getActivity().findViewById(R.id.x8);
        x9= (TextView) getActivity().findViewById(R.id.x9);
        x10= (TextView) getActivity().findViewById(R.id.x10);
        yhq= (GifImageView) getActivity().findViewById(R.id.yhq);
        final EditText ed= getActivity().findViewById(R.id.searchedit);
        Button bu= getActivity().findViewById(R.id.searchbutton);
        t1 = getActivity().findViewById(R.id.t1);
        t2 = getActivity().findViewById(R.id.t2);
        t3 = getActivity().findViewById(R.id.t3);
        t4 = getActivity().findViewById(R.id.t4);
        t5 = getActivity().findViewById(R.id.t5);
        t6 = getActivity().findViewById(R.id.t6);
        tt1 = getActivity().findViewById(R.id.tt1);
        tt2 = getActivity().findViewById(R.id.tt2);
        tt3 = getActivity().findViewById(R.id.tt3);
        tt4 = getActivity().findViewById(R.id.tt4);
        tt5 = getActivity().findViewById(R.id.tt5);
        tt6 = getActivity().findViewById(R.id.tt6);
        ttt1 = getActivity().findViewById(R.id.ttt1);
        ttt2 = getActivity().findViewById(R.id.ttt2);
        ttt3 = getActivity().findViewById(R.id.ttt3);
        ttt4 = getActivity().findViewById(R.id.ttt4);
        ttt5 = getActivity().findViewById(R.id.ttt5);
        ttt6 = getActivity().findViewById(R.id.ttt6);
        cd = getActivity().findViewById(R.id.cd);
        cq = getActivity().findViewById(R.id.cq);
        xa = getActivity().findViewById(R.id.xa);
        sh = getActivity().findViewById(R.id.sh);

        @SuppressLint("HandlerLeak")
        final Handler handler =new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                List<HashMap<String,Object>> mlist= (List<HashMap<String, Object>>) msg.obj;
                Iterator<HashMap<String, Object>> its = mlist.iterator();
                String photo =null;
                String name =null;
                String type =null;
                String m =null;
                String[] descA = new String[mlist.size()];
                String[] descB = new String[mlist.size()];
                final String[] descC = new String[mlist.size()];
                final String[] descD = new String[mlist.size()];
                int p = 0;
                while(its.hasNext()){
                    HashMap<String, Object> map = new HashMap<String, Object>();
                    map=its.next();
                    photo= (String) map.get("photo");
                    descA[p] =photo;
                    name= (String) map.get("name");
                    descB[p] =name;
                    type= (String) map.get("type");
                    descC[p] =type;
                    m= (String) map.get("price");
                    descD[p] =m;
                    p++;
                }
                t1.setBackgroundResource(Integer.parseInt(descA[2]));
                tt1.setText(descB[2]);
                ttt1.setText(descD[2]);
                t2.setBackgroundResource(Integer.parseInt(descA[1]));
                tt2.setText(descB[1]);
                ttt2.setText(descD[1]);
                t3.setBackgroundResource(Integer.parseInt(descA[0]));
                tt3.setText(descB[0]);
                ttt3.setText(descD[0]);
                t4.setBackgroundResource(Integer.parseInt(descA[3]));
                tt4.setText(descB[3]);
                ttt4.setText(descD[3]);
                t5.setBackgroundResource(Integer.parseInt(descA[4]));
                tt5.setText(descB[4]);
                ttt5.setText(descD[4]);
                t6.setBackgroundResource(Integer.parseInt(descA[7]));
                tt6.setText(descB[7]);
                ttt6.setText(descD[7]);

            }
        };
        Thread thread=new Thread(new Runnable() {
            List<HashMap<String,Object>> list1=new ArrayList<HashMap<String, Object>>();
            @Override
            public void run() {
                Mydatabases.getConnection();
                try {
                    list1=Mydatabases.getgoodsall();
                    Log.d(TAG, list1.toString());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Message message=Message.obtain();
                message.what=1;
                message.obj=list1;
                handler.sendMessage(message);
            }
        });
        thread.start();


        bu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), Search.class);
                String a = ed.getText().toString();
                intent.putExtra("search",a);
                startActivity(intent);
            }
        });
        yhq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyDialog2 myDialog = new MyDialog2(getActivity(), R.style.MyDialog);
                myDialog.setTitle("Message")
                        .setMessage("Are you sure to sign in?")
                        .setCancel("cancel", dialog -> dialog.dismiss())
                        .setConfirm("yes", dialog -> {
                            SharedPreferences sp =  getActivity().getSharedPreferences("name", MODE_PRIVATE);
                            String b = sp.getString("name", null);
                            SharedPreferences sp1 =  getActivity().getSharedPreferences("allyhq", MODE_PRIVATE);
                            int pd= sp1.getInt("yhq", 0);
                            SharedPreferences sp2 =  getActivity().getSharedPreferences("allyhq", MODE_PRIVATE);
                            SharedPreferences.Editor editor = sp2.edit();
                            int pd1=pd+1;
                            editor.putInt("yhq", pd1);
                            editor.commit();
                            Toast.makeText(getActivity(), "You have successfully claimed a coupon.", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();

                        })
                        .show();
            }
        });

        t1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), Mygoods.class);
                intent.putExtra("id",3);
                startActivity(intent);
            }
        });
        t2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), Mygoods.class);
                intent.putExtra("id",2);
                startActivity(intent);
            }
        });

        t3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), Mygoods.class);
                intent.putExtra("id",1);
                startActivity(intent);
            }
        });

        t4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), Mygoods.class);
                intent.putExtra("id",4);
                startActivity(intent);
            }
        });

        t5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), Mygoods.class);
                intent.putExtra("id",5);
                startActivity(intent);
            }
        });

        t6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), Mygoods.class);
                intent.putExtra("id",8);
                startActivity(intent);
            }
        });

        Fragment banner = new A1();
        //实例化管理器
        FragmentManager fragmentManager1 = getFragmentManager();
        //定义事务
        FragmentTransaction fta1 = fragmentManager1.beginTransaction();
        //添加fragment
        fta1.add(R.id.fag4, banner);
        //提交事务`
        fta1.commit();

        Fragment c1 = new Index_b1();
        //实例化管理器
        FragmentManager fragmentManager = getFragmentManager();
        //定义事务
        FragmentTransaction fta = fragmentManager.beginTransaction();
        //添加fragment
        fta.add(R.id.fag3, c1);
        //提交事务`
        fta.commit();
        cd.setTextColor(Color.parseColor("#5bad56"));
        cq.setTextColor(Color.parseColor("#4f5555"));
        xa.setTextColor(Color.parseColor("#4f5555"));
        sh.setTextColor(Color.parseColor("#4f5555"));


        cd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //实例化fragment
                Fragment c1 = new Index_b1();
                //实例化管理器
                FragmentManager fragmentManager = getFragmentManager();
                //定义事务
                FragmentTransaction fta = fragmentManager.beginTransaction();
                //添加fragment
                fta.replace(R.id.fag3, c1);
                //提交事务`
                fta.commit();
                cd.setTextColor(Color.parseColor("#5bad56"));
                cq.setTextColor(Color.parseColor("#4f5555"));
                xa.setTextColor(Color.parseColor("#4f5555"));
                sh.setTextColor(Color.parseColor("#4f5555"));

            }
        });

        cq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //实例化fragment
                Fragment c2 = new Index_b2();
                //实例化管理器
                FragmentManager fragmentManager = getFragmentManager();
                //定义事务
                FragmentTransaction fta = fragmentManager.beginTransaction();
                //添加fragment
                fta.replace(R.id.fag3, c2);
                //提交事务`
                fta.commit();
                cq.setTextColor(Color.parseColor("#5bad56"));
                cd.setTextColor(Color.parseColor("#4f5555"));
                xa.setTextColor(Color.parseColor("#4f5555"));
                sh.setTextColor(Color.parseColor("#4f5555"));
            }
        });
        xa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //实例化fragment
                Fragment c3 = new Index_b3();
                //实例化管理器
                FragmentManager fragmentManager = getFragmentManager();
                //定义事务
                FragmentTransaction fta = fragmentManager.beginTransaction();
                //添加fragment
                fta.replace(R.id.fag3, c3);
                //提交事务`
                fta.commit();
                xa.setTextColor(Color.parseColor("#5bad56"));
                cd.setTextColor(Color.parseColor("#4f5555"));
                cq.setTextColor(Color.parseColor("#4f5555"));
                sh.setTextColor(Color.parseColor("#4f5555"));

            }
        });
        sh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //实例化fragment
                Fragment c4 = new Index_b4();
                //实例化管理器
                FragmentManager fragmentManager = getFragmentManager();
                //定义事务
                FragmentTransaction fta = fragmentManager.beginTransaction();
                //添加fragment
                fta.replace(R.id.fag3, c4);
                //提交事务`
                fta.commit();
                sh.setTextColor(Color.parseColor("#5bad56"));
                cq.setTextColor(Color.parseColor("#4f5555"));
                cd.setTextColor(Color.parseColor("#4f5555"));
                xa.setTextColor(Color.parseColor("#4f5555"));

            }
        });

        x1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("id", 2);//其中1表示Fragment的编号，从0开始编，secondFra的编号为1
                intent.setClass(getContext(), Home.class);
                startActivity(intent);
                


            }
        });
    }
}

