package com.example.yuanann.vegetable_app;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class Index_b1 extends Fragment {
    private static final String  TAG="DBUtils";
    private GridView listView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.activity_index_b1,container,false);
        return view;
    }
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        super.onCreate(savedInstanceState);
//        GridView list2=(GridView)getActivity().findViewById(R.id.grid);

        @SuppressLint("HandlerLeak")
        final Handler handler =new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                listView= getActivity().findViewById(R.id.grid);
                List<HashMap<String,Object>> mlist= (List<HashMap<String, Object>>) msg.obj;
                Iterator<HashMap<String, Object>> its = mlist.iterator();
                String id =null;
                final String[] descD = new String[mlist.size()];
                int p = 0;
                while(its.hasNext()){
                    HashMap<String, Object> map = new HashMap<String, Object>();
                    map=its.next();
                    id= (String) map.get("id");
//                    System.out.print(id+"\n");
                    descD[p] =id;
                    p++;
                }
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        int cl= Integer.parseInt((descD[i]));
                        Intent intent=new Intent(getContext(),Mygoods.class);
                        intent.putExtra("id",cl);
                        startActivity(intent);
                    }
                });
                if(msg.what==1){
                    List<HashMap<String,Object>> list2= (List<HashMap<String, Object>>) msg.obj;
                    SimpleAdapter simpleAdapter=new SimpleAdapter(getContext(),list2,R.layout.layout_index,new String[]{"photo","name","price"},new int[]{R.id.a1,R.id.a2,R.id.a3});
                    listView.setAdapter(simpleAdapter);
                }
            }
        };
        Thread thread=new Thread(new Runnable() {
            List<HashMap<String,Object>> list1=new ArrayList<HashMap<String, Object>>();
            @Override
            public void run() {
                Mydatabases.getConnection();
                try {
                    list1=Mydatabases.getgoods();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Message message=Message.obtain();
                message.what=1;
                message.obj=list1;
                handler.sendMessage(message);
            }
        });
        thread.start();


    }
}