package com.example.yuanann.vegetable_app;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class Loading extends AppCompatActivity {
    private static final String  TAG="DBUtils";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        welcome();
        setContentView(R.layout.activity_loading);

        SharedPreferences sp = Loading.this.getSharedPreferences("name", MODE_PRIVATE);
        final String name = sp.getString("name", null);
        @SuppressLint("HandlerLeak")
        final Handler handler =new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                List<HashMap<String,Object>> mlist= (List<HashMap<String, Object>>) msg.obj;
                Iterator<HashMap<String, Object>> its = mlist.iterator();
                String add =null;
                final String[] descD = new String[mlist.size()];
                int p = 0;
                while(its.hasNext()){
                    HashMap<String, Object> map = new HashMap<String, Object>();
                    map=its.next();
                    add= (String) map.get("add");
                    descD[p] =add;
                    p++;
                }
                if(p==0){
                    SharedPreferences sp = getSharedPreferences("name", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("add",null);
                    editor.commit();
                }else{
                    SharedPreferences sp = getSharedPreferences("name", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("add",descD[0]);
                    editor.commit();
                }

            }
        };
        Thread thread=new Thread(new Runnable() {
            List<HashMap<String,Object>> list1=new ArrayList<HashMap<String, Object>>();
            @Override
            public void run() {
                Mydatabases.getConnection();
                try {
                    list1=Mydatabases.geUser(name);
                    Log.d(TAG, list1.toString());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Message message=Message.obtain();
                message.what=1;
                message.obj=list1;
                handler.sendMessage(message);
            }
        });
        thread.start();

    }

    private void welcome() {
        Handler handler = new Handler();
        //当计时结束,跳转至主界面
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent();
                intent.setClass(Loading.this,Home.class);
                startActivity(intent);
            }
        }, 4000);
    }
}
