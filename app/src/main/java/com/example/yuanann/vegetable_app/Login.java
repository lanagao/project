package com.example.yuanann.vegetable_app;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class Login extends AppCompatActivity {
    private static final String  TAG="DBUtils";
    Button b1;
    EditText e1,e2;
    TextView b2,yk,admin;
    String n,p;
    String aa="n";
    String st="y";
    String ps=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //设置当前页面的布局初始化组件
        setContentView(R.layout.activity_login);
        //
        b1 = (Button) findViewById(R.id.b1);
        b2 = (TextView) findViewById(R.id.b2);
        yk = (TextView) findViewById(R.id.yk);
        e1 = (EditText) findViewById(R.id.edit1);
        e2 = (EditText) findViewById(R.id.edit2);
        admin = (TextView) findViewById(R.id.admin);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String n = e1.getText().toString();
                String pass = e2.getText().toString();
                @SuppressLint("HandlerLeak")
                final Handler handler2 =new Handler(){
                    @Override
                    public void handleMessage(@NonNull Message msg) {
                        super.handleMessage(msg);
                        final ListView listView=Login.this.findViewById(R.id.list);
                        List<HashMap<String,Object>> mlist= (List<HashMap<String, Object>>) msg.obj;
                        Iterator<HashMap<String, Object>> its = mlist.iterator();
                        String id =null;
                        String state =null;
                        String psd =null;
                        final String[] descA = new String[mlist.size()];
                        final String[] descD = new String[mlist.size()];
                        final String[] descB = new String[mlist.size()];
                        int p = 0;
                        while(its.hasNext()){
                            HashMap<String, Object> map = new HashMap<String, Object>();
                            map=its.next();
                            id= (String) map.get("id");
                            descD[p] =id;
                            state= (String) map.get("state");
                            descA[p] =state;
                            psd= (String) map.get("psd");
                            descB[p] =psd;
                            p++;
                        }
                        if(p==0){
                        }else{
                            aa="y";
                            st=descA[0];
                            ps=descB[0];
                        }
                        if (TextUtils.isEmpty(n)) {
                            Toast.makeText(getApplicationContext(), "Please enter username", Toast.LENGTH_SHORT).show();
                        } else if (TextUtils.isEmpty(pass)) {
                            Toast.makeText(getApplicationContext(), "Please enter password", Toast.LENGTH_SHORT).show();
                        } else if (aa.equals("n")) {
                            Toast.makeText(getApplicationContext(), "The username doesn't exist", Toast.LENGTH_SHORT).show();
                        }else if (st.equals("n")){
                            Toast.makeText(getApplicationContext(), "Sorry, you are not allowed to login", Toast.LENGTH_SHORT).show();
                        } else if (pass.equals(ps)) {
                            Toast.makeText(getApplicationContext(), "Login successful", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent();
                            intent.setClass(Login.this, Loading.class);
                            SharedPreferences sp = getSharedPreferences("name", MODE_PRIVATE);
                            SharedPreferences.Editor editor = sp.edit();
                            editor.putString("name", n);
                            editor.commit();
                            startActivity(intent);
                            e1.setText("");
                            e2.setText("");
                        } else {
                            Toast.makeText(getApplicationContext(), "Login failed, wrong password", Toast.LENGTH_SHORT).show();
                            e1.setText("");
                            e2.setText("");
                        }
                    }
                };
                Thread thread2=new Thread(new Runnable() {
                    List<HashMap<String,Object>> list1=new ArrayList<HashMap<String, Object>>();
                    @Override
                    public void run() {
                        Mydatabases.getConnection();
                        try {
                            list1=Mydatabases.geUsername(n);
                            Log.d(TAG, list1.toString());
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        Message message=Message.obtain();
                        message.what=1;
                        message.obj=list1;
                        handler2.sendMessage(message);
                    }
                });
                thread2.start();

            }
        });
        yk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sp = getSharedPreferences("name", MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.putString("name", null);
                editor.putString("add", null);
                editor.commit();
                Intent intent = new Intent();
                intent.setClass(Login.this, Home.class);
                startActivity(intent);
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(Login.this, Register.class);
                startActivity(intent);
            }
        });
        admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(Login.this, Back_login.class);
                startActivity(intent);
            }
        });

    }
}
