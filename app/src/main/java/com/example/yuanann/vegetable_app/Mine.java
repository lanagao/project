package com.example.yuanann.vegetable_app;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;


public class Mine extends Fragment {
    private static final String  TAG="DBUtils";
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.activity_mine,container,false);
        return view;
    }
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        super.onCreate(savedInstanceState);
        TextView myyhq=(TextView)getActivity().findViewById(R.id.myyhq);
        TextView myjf=(TextView)getActivity().findViewById(R.id.myjf);
        ImageView lqjf=(ImageView) getActivity().findViewById(R.id.lqjf);
        TextView a1=(TextView)getActivity().findViewById(R.id.a1);
        TextView a2=(TextView)getActivity().findViewById(R.id.a2);
        TextView a3=(TextView)getActivity().findViewById(R.id.a3);
        TextView a4=(TextView)getActivity().findViewById(R.id.a4);
        TextView a5=(TextView)getActivity().findViewById(R.id.a5);
        TextView h1=(TextView)getActivity().findViewById(R.id.fouce);
        TextView h2=(TextView)getActivity().findViewById(R.id.fouce1);
        TextView name=(TextView) getActivity().findViewById(R.id.name);
        TextView back=(TextView)getActivity().findViewById(R.id.back);

        SharedPreferences sp =  getActivity().getSharedPreferences("name", MODE_PRIVATE);
        String myname=sp.getString("name", null);

        @SuppressLint("HandlerLeak")
        final Handler handler2 =new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                final ListView listView= getActivity().findViewById(R.id.list);
                List<HashMap<String,Object>> mlist= (List<HashMap<String, Object>>) msg.obj;
                Iterator<HashMap<String, Object>> its = mlist.iterator();
                String id =null;
                final String[] descD = new String[mlist.size()];
                int p = 0;
                while(its.hasNext()){
                    HashMap<String, Object> map = new HashMap<String, Object>();
                    map=its.next();
                    id= (String) map.get("id");
//                    System.out.print(id+"\n");
                    descD[p] =id;
                    p++;
                }
                h1.setText(p+"");

            }
        };
        Thread thread2=new Thread(new Runnable() {
            List<HashMap<String,Object>> list1=new ArrayList<HashMap<String, Object>>();
            @Override
            public void run() {
                Mydatabases.getConnection();
                try {
                    list1=Mydatabases.getFouce(myname);
                    Log.d(TAG, list1.toString());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Message message=Message.obtain();
                message.what=1;
                message.obj=list1;
                handler2.sendMessage(message);
            }
        });
        thread2.start();

        if(sp.getString("name", null)==null){
            String b="You haven't login, click to login";
            name.setText(b);
            name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent();
                    intent.setClass(getActivity(), Login.class);
                    startActivity(intent);
                }
            });
        }else {
            String b = "Welcome，" + sp.getString("name", null);
            name.setText(b);
        }
        SharedPreferences sp1 =  getActivity().getSharedPreferences("allyhq", MODE_PRIVATE);
        int num =sp1.getInt("yhq", 0);
        String yhqnum=num+"";
        myyhq.setText(yhqnum);
//        SharedPreferences sp2 =  getActivity().getSharedPreferences("alljf", MODE_PRIVATE);
//        int num2 =sp2.getInt("jf", 0);
//        String yhqnum2=num2+"";
//        myjf.setText(yhqnum2);

        Handler handler = new Handler() {
            public void handleMessage(android.os.Message msg) {
                if (msg.what == 0&&isAdded()) {
                    SharedPreferences sp2 = getActivity().getSharedPreferences("alljf", MODE_PRIVATE);
                    int num2 = sp2.getInt("jf", 0);
                    String yhqnum2 = num2 + "";
                    myjf.setText(yhqnum2);
                }
                }
            };

            new Thread(new Runnable() {

                @Override
                public void run() {
                    while (true) {
                        handler.sendEmptyMessage(0);

                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }).start();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), Login.class);
                startActivity(intent);
            }
        });

        h1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), Fouce.class);
                startActivity(intent);
            }
        });

        h2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), Fouce.class);
                startActivity(intent);
            }
        });

        a1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(myname==null) {
                    Toast.makeText(getContext(), "Please login", Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent = new Intent();
                    intent.setClass(getActivity(), Order.class);
                    startActivity(intent);
                }
            }
        });

       a2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(myname==null) {
                    Toast.makeText(getContext(), "Please login", Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent = new Intent();
                    intent.setClass(getActivity(), Order.class);
                    startActivity(intent);
                }
            }
        });

        a3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(myname==null) {
                    Toast.makeText(getContext(), "Please login", Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent = new Intent();
                    intent.setClass(getActivity(), Order.class);
                    startActivity(intent);
                }
            }
        });

        a4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(myname==null) {
                    Toast.makeText(getContext(), "Please login", Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent = new Intent();
                    intent.setClass(getActivity(), Order.class);
                    startActivity(intent);
                }
            }
        });

        a5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(myname==null) {
                    Toast.makeText(getContext(), "Please login", Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent = new Intent();
                    intent.setClass(getActivity(), Myaddress.class);
                    startActivity(intent);
                }
            }
        });

        lqjf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                            SharedPreferences sp1 =  getActivity().getSharedPreferences("alljf", MODE_PRIVATE);
                            int myjf= sp1.getInt("jf", 0);
                            SharedPreferences sp2 =  getActivity().getSharedPreferences("alljf", MODE_PRIVATE);
                            SharedPreferences.Editor editor = sp2.edit();
                            int myjf1=myjf+5;
                            editor.putInt("jf", myjf1);
                            editor.commit();
                            Toast.makeText(getActivity(), "Receive 5 points successfully", Toast.LENGTH_SHORT).show();
            }
        });

        }
    }

