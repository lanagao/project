package com.example.yuanann.vegetable_app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * 使用接口回调的方式实现button点击事件.
 *
 * @author xuzhuyun
 */
@SuppressWarnings("unused")
public class MyAdapter extends BaseAdapter implements View.OnClickListener {
    List<String> mStringList = new ArrayList();
    private Context mContext;
    private Callback mCallback;

    public MyAdapter(Context context, Callback callback) {
        mContext = context;
        mCallback = callback;
    }

    public void setStringList(List<String> stringList) {
        mStringList = stringList;
    }

    public List<String> getStringList() {
        return mStringList;
    }

    @Override
    public int getCount() {
        return mStringList.size();
    }

    @Override
    public Object getItem(int i) {
        return getCount() == 0 ? null : mStringList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.layout1, null);
            viewHolder = new ViewHolder();
            viewHolder.mTvItemName = view.findViewById(R.id.a2);
            viewHolder.mBtnClick = view.findViewById(R.id.bu1);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.mTvItemName.setText(mStringList.get(i));
        viewHolder.mBtnClick.setOnClickListener(this);
        viewHolder.mBtnClick.setTag(i);//保存当前点击按钮的位置
        return view;
    }


    class ViewHolder {
        TextView mTvItemName;
        Button mBtnClick;
    }


    @Override
    public void onClick(View view) {
        mCallback.click(view);
    }

    /**
     * 回调接口.
     */
    public interface Callback {
        void click(View v);
    }
}
