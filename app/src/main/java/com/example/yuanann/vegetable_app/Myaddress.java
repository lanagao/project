package com.example.yuanann.vegetable_app;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class Myaddress extends AppCompatActivity {
    private static final String  TAG="DBUtils";
    Button b1;
    EditText ed;
    String n, p, t, i;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myaddress);
        ImageView back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Myaddress.this.finish();
            }
        });
        b1 = (Button) findViewById(R.id.bu1);
        ed = (EditText) findViewById(R.id.ed);

        SharedPreferences sp = Myaddress.this.getSharedPreferences("name", MODE_PRIVATE);
        final String name = sp.getString("name", null);
        @SuppressLint("HandlerLeak")
        final Handler handler =new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                List<HashMap<String,Object>> mlist= (List<HashMap<String, Object>>) msg.obj;
                Iterator<HashMap<String, Object>> its = mlist.iterator();
                String add =null;
                final String[] descD = new String[mlist.size()];
                int p = 0;
                while(its.hasNext()){
                    HashMap<String, Object> map = new HashMap<String, Object>();
                    map=its.next();
                    add= (String) map.get("add");
                    descD[p] =add;
                    p++;
                }
                ed.setText(descD[0]);
            }
        };
        Thread thread=new Thread(new Runnable() {
            List<HashMap<String,Object>> list1=new ArrayList<HashMap<String, Object>>();
            @Override
            public void run() {
                Mydatabases.getConnection();
                try {
                    list1=Mydatabases.geUser(name);
                    Log.d(TAG, list1.toString());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Message message=Message.obtain();
                message.what=1;
                message.obj=list1;
                handler.sendMessage(message);
            }
        });
        thread.start();

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i = ed.getText().toString().trim();
                int l = i.length();
                if (TextUtils.isEmpty(i)) {
                    Toast.makeText(Myaddress.this, "Please enter your personal signature", Toast.LENGTH_SHORT).show();
                } else if (l > 2000) {
                    Toast.makeText(Myaddress.this, "The address is too long", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(Myaddress.this, "Change address successful", Toast.LENGTH_SHORT).show();
                    DatabaseMydapter database = new DatabaseMydapter();
                    database.onUpdateAdd(name,i);
                    SharedPreferences sp = getSharedPreferences("name", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("add",i);
                    editor.commit();
                    Intent intent = getIntent();
                    intent.putExtra("id", 5);//其中1表示Fragment的编号，从0开始编，secondFra的编号为1
                    intent.setClass(Myaddress.this, Home.class);
                    startActivity(intent);
                }
            }
        });
    }
}