package com.example.yuanann.vegetable_app;

import android.database.Cursor;
import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Mydatabases {
    private static final String TAG = "DBUtils";
    private static Connection conn = null;

    public static Connection getConnection() {
        String ip = "10.0.2.2";
        int port = 3306;
        String user = "root";
        String password = "abc123";
        String url = "jdbc:mysql://" + ip + ":" + port + "/" + "myvegetable" + "?useUnicode=true&characterEncoding=UTF-8&useSSL=false";
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Log.d(TAG, "加载JDBC驱动成功");
        } catch (ClassNotFoundException e) {
            Log.d(TAG, "加载JDBC驱动失败");
        }
        try {
            conn = DriverManager.getConnection(url, user, password);
            Log.d(TAG, "数据库连接成功");

        } catch (SQLException e) {
            Log.d(TAG, "数据库连接失败");
        }
        return conn;
    }

    public static List<HashMap<String, Object>> getgoods() throws SQLException {
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        Connection conn = getConnection();
        Statement sta = conn.createStatement();
        String sql = "select * from mygoods where g_id <=8";
        ResultSet result = sta.executeQuery(sql);
        if (result == null) {
            return null;
        }
        while (result.next()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("id", result.getString("g_id"));
            map.put("photo", result.getString("g_photo"));
            map.put("name", result.getString("g_name"));
            map.put("type", result.getString("g_type"));
            map.put("price", result.getString("g_price"));
            list.add(map);
        }
        return list;
    }

    public static List<HashMap<String, Object>> getgoodsall() throws SQLException {
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        Connection conn = getConnection();
        Statement sta = conn.createStatement();
        String sql = "select * from mygoods";
        ResultSet result = sta.executeQuery(sql);
        if (result == null) {
            return null;
        }
        while (result.next()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("id", result.getString("g_id"));
            map.put("photo", result.getString("g_photo"));
            map.put("name", result.getString("g_name"));
            map.put("lei", result.getString("g_lei"));
            map.put("type", result.getString("g_type"));
            map.put("price", result.getString("g_price"));
            list.add(map);
        }
        return list;
    }

    public static List<HashMap<String, Object>> getgoods2() throws SQLException {
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        Connection conn = getConnection();
        Statement sta = conn.createStatement();
        String sql = "select * from mygoods where g_id <=16 and g_id > 8";
        ResultSet result = sta.executeQuery(sql);
        if (result == null) {
            return null;
        }
        while (result.next()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("id", result.getString("g_id"));
            map.put("photo", result.getString("g_photo"));
            map.put("name", result.getString("g_name"));
            map.put("type", result.getString("g_type"));
            map.put("price", result.getString("g_price"));
            list.add(map);
        }
        return list;
    }

    public static List<HashMap<String, Object>> getgoods3() throws SQLException {
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        Connection conn = getConnection();
        Statement sta = conn.createStatement();
        String sql = "select * from mygoods where g_id <=24 and g_id > 16";
        ResultSet result = sta.executeQuery(sql);
        if (result == null) {
            return null;
        }
        while (result.next()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("id", result.getString("g_id"));
            map.put("photo", result.getString("g_photo"));
            map.put("name", result.getString("g_name"));
            map.put("type", result.getString("g_type"));
            map.put("price", result.getString("g_price"));
            list.add(map);
        }
        return list;
    }

    public static List<HashMap<String, Object>> getgoods4() throws SQLException {
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        Connection conn = getConnection();
        Statement sta = conn.createStatement();
        String sql = "select * from mygoods where g_id <=32 and g_id > 24";
        ResultSet result = sta.executeQuery(sql);
        if (result == null) {
            return null;
        }
        while (result.next()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("id", result.getString("g_id"));
            map.put("photo", result.getString("g_photo"));
            map.put("name", result.getString("g_name"));
            map.put("type", result.getString("g_type"));
            map.put("price", result.getString("g_price"));
            list.add(map);
        }
        return list;
    }

    public static List<HashMap<String, Object>> getgoods5() throws SQLException {
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        Connection conn = getConnection();
        Statement sta = conn.createStatement();
        String sql = "select * from mygoods where g_id <=40 and g_id > 32";
        ResultSet result = sta.executeQuery(sql);
        if (result == null) {
            return null;
        }
        while (result.next()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("id", result.getString("g_id"));
            map.put("photo", result.getString("g_photo"));
            map.put("name", result.getString("g_name"));
            map.put("type", result.getString("g_type"));
            map.put("price", result.getString("g_price"));
            list.add(map);
        }
        return list;
    }

    public static List<HashMap<String, Object>> getgoods6() throws SQLException {
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        Connection conn = getConnection();
        Statement sta = conn.createStatement();
        String sql = "select * from mygoods where g_id <=50 and g_id > 42";
        ResultSet result = sta.executeQuery(sql);
        if (result == null) {
            return null;
        }
        while (result.next()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("id", result.getString("g_id"));
            map.put("photo", result.getString("g_photo"));
            map.put("name", result.getString("g_name"));
            map.put("type", result.getString("g_type"));
            map.put("price", result.getString("g_price"));
            list.add(map);
        }
        return list;
    }

    public static List<HashMap<String, Object>> getgoods7() throws SQLException {
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        Connection conn = getConnection();
        Statement sta = conn.createStatement();
        String sql = "select * from mygoods where g_id <=58 and g_id > 50";
        ResultSet result = sta.executeQuery(sql);
        if (result == null) {
            return null;
        }
        while (result.next()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("id", result.getString("g_id"));
            map.put("photo", result.getString("g_photo"));
            map.put("name", result.getString("g_name"));
            map.put("type", result.getString("g_type"));
            map.put("price", result.getString("g_price"));
            list.add(map);
        }
        return list;
    }

    public static List<HashMap<String, Object>> geUsername(String name) throws SQLException {
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        Connection conn = getConnection();
        Statement sta = conn.createStatement();
        String sql = "select * from user where u_name='"+name+"'";
        ResultSet result = sta.executeQuery(sql);
        if (result == null) {
            return null;
        }
        while (result.next()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("add", result.getString("u_add"));
            map.put("state", result.getString("u_state"));
            map.put("psd", result.getString("u_psd"));
            map.put("id", result.getString("u_id"));
            list.add(map);
        }
        return list;
    }

    public static List<HashMap<String, Object>> geUserId(String id) throws SQLException {
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        Connection conn = getConnection();
        Statement sta = conn.createStatement();
        String sql = "select * from user where u_id='"+id+"'";
        ResultSet result = sta.executeQuery(sql);
        if (result == null) {
            return null;
        }
        while (result.next()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("add", result.getString("u_add"));
            map.put("name", result.getString("u_name"));
            map.put("state", result.getString("u_state"));
            map.put("psd", result.getString("u_psd"));
            map.put("id", result.getString("u_id"));
            list.add(map);
        }
        return list;
    }

    public static List<HashMap<String, Object>> geUsername() throws SQLException {
        String table="user";
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        Connection conn = getConnection();
        Statement sta = conn.createStatement();
        String sql = "select * from "+table;
        ResultSet result = sta.executeQuery(sql);
        if (result == null) {
            return null;
        }
        while (result.next()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("add", result.getString("u_add"));
            map.put("state", result.getString("u_state"));
            map.put("psd", result.getString("u_psd"));
            map.put("name", result.getString("u_name"));
            map.put("id", result.getString("u_id"));
            map.put("time", result.getString("u_time"));
            list.add(map);
        }
        return list;
    }

    public static List<HashMap<String, Object>> geUser(String name) throws SQLException {
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        Connection conn = getConnection();
        Statement sta = conn.createStatement();
        String sql = "select * from user where u_name='"+name+"'";
        ResultSet result = sta.executeQuery(sql);
        if (result == null) {
            return null;
        }
        while (result.next()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("add", result.getString("u_add"));
            list.add(map);
        }
        return list;
    }

    public static List<HashMap<String, Object>> getgoods8() throws SQLException {
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        Connection conn = getConnection();
        Statement sta = conn.createStatement();
        String sql = "select * from mygoods where g_id <=66 and g_id > 58";
        ResultSet result = sta.executeQuery(sql);
        if (result == null) {
            return null;
        }
        while (result.next()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("id", result.getString("g_id"));
            map.put("photo", result.getString("g_photo"));
            map.put("name", result.getString("g_name"));
            map.put("type", result.getString("g_type"));
            map.put("price", result.getString("g_price"));
            list.add(map);
        }
        return list;
    }

    public static List<HashMap<String, Object>> getgoods9() throws SQLException {
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        Connection conn = getConnection();
        Statement sta = conn.createStatement();
        String sql = "select * from mygoods where g_id <=74 and g_id > 66";
        ResultSet result = sta.executeQuery(sql);
        if (result == null) {
            return null;
        }
        while (result.next()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("id", result.getString("g_id"));
            map.put("photo", result.getString("g_photo"));
            map.put("name", result.getString("g_name"));
            map.put("type", result.getString("g_type"));
            map.put("price", result.getString("g_price"));
            list.add(map);
        }
        return list;
    }

    public static List<HashMap<String, Object>> getgoodsall(int id) throws SQLException {
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        Connection conn = getConnection();
        Statement sta = conn.createStatement();
        String sql = "select * from mygoods where g_id="+id;
        ResultSet result = sta.executeQuery(sql);
        if (result == null) {
            return null;
        }
        while (result.next()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("id", result.getString("g_id"));
            map.put("photo", result.getString("g_photo"));
            map.put("name", result.getString("g_name"));
            map.put("type", result.getString("g_type"));
            map.put("lei", result.getString("g_lei"));
            map.put("price", result.getString("g_price"));
            list.add(map);
        }
        return list;
    }

    public static List<HashMap<String, Object>> getCart(String name) throws SQLException {
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        Connection conn = getConnection();
        Statement sta = conn.createStatement();
        String sql = "select * from cart where user='"+name+"'";
        ResultSet result = sta.executeQuery(sql);
        if (result == null) {
            return null;
        }
        while (result.next()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("id", result.getInt("g_id"));
            map.put("photo", result.getInt("g_photo"));
            map.put("name", result.getString("g_name"));
            map.put("type", result.getString("g_type"));
            map.put("price", result.getDouble("g_price"));
            map.put("num", result.getInt("g_num"));
            map.put("check", result.getString("g_check"));
            map.put("user", result.getString("user"));
            map.put("date", result.getString("date"));
            list.add(map);
        }
        return list;
    }

    public static List<HashMap<String, Object>> getfind(String fname) throws SQLException {
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        Connection conn = getConnection();
        Statement sta = conn.createStatement();
        String sql = "select * from tb_flower where fname='"+fname+"'";
        ResultSet result = sta.executeQuery(sql);
        if (result == null) {
            return null;
        }
        while (result.next()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("info", result.getString("info"));
            map.put("user", result.getString("user"));
            map.put("date", result.getString("date"));
            list.add(map);
        }
        return list;
    }

    public static List<HashMap<String, Object>> getfind() throws SQLException {
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        Connection conn = getConnection();
        Statement sta = conn.createStatement();
        String sql = "select * from tb_flower";
        ResultSet result = sta.executeQuery(sql);
        if (result == null) {
            return null;
        }
        while (result.next()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("info", result.getString("info"));
            map.put("id", result.getString("t_id"));
            map.put("user", result.getString("user"));
            map.put("fname", result.getString("fname"));
            map.put("date", result.getString("date"));
            list.add(map);
        }
        return list;
    }

    public static List<HashMap<String, Object>> getFouce(String name) throws SQLException {
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        Connection conn = getConnection();
        Statement sta = conn.createStatement();
        String sql = "select * from userfouce where user='"+name+"'";
        ResultSet result = sta.executeQuery(sql);
        if (result == null) {
            return null;
        }
        while (result.next()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("photo", result.getString("f_photo"));
            map.put("name", result.getString("f_name"));
            map.put("type", result.getString("f_type"));
            map.put("id", result.getString("f_id"));
            list.add(map);
        }
        return list;
    }

    public static List<HashMap<String, Object>> getWaitcheck() throws SQLException {
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        Connection conn = getConnection();
        Statement sta = conn.createStatement();
        String sql = "select * from waitorder where g_check='true'";
        ResultSet result = sta.executeQuery(sql);
        if (result == null) {
            return null;
        }
        while (result.next()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("id", result.getString("g_id"));
            map.put("photo", result.getString("g_photo"));
            map.put("name", result.getString("g_name"));
            map.put("type", result.getString("g_type"));
            map.put("price", result.getString("g_price"));
            map.put("num", result.getString("g_num"));
            map.put("check", result.getString("g_check"));
            map.put("user", result.getString("user"));
            map.put("date", result.getString("date"));
            list.add(map);
        }
        return list;
    }

    public static List<HashMap<String, Object>> getSearch(String name) throws SQLException {
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        Connection conn = getConnection();
        Statement sta = conn.createStatement();
        String sql = "select * from mygoods where g_name like '%"+name+"%'";
        ResultSet result = sta.executeQuery(sql);
        if (result == null) {
            return null;
        }
        while (result.next()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("id", result.getString("g_id"));
            map.put("photo", result.getString("g_photo"));
            map.put("name", result.getString("g_name"));
            map.put("type", result.getString("g_type"));
            map.put("price", result.getString("g_price"));
            list.add(map);
        }
        return list;
    }

    public static List<HashMap<String, Object>> getMyorder(String name) throws SQLException {
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        Connection conn = getConnection();
        Statement sta = conn.createStatement();
//        String sql = "select * from myorder o1,waitorder o2 where o1.user='"+name+"'and o2.user='"+name+"'";
        String sql = "select * from myorder where user='"+name+"'";
        ResultSet result = sta.executeQuery(sql);
        if (result == null) {
            return null;
        }
        while (result.next()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("id", result.getString("g_id"));
            map.put("photo", result.getString("g_photo"));
            map.put("name", result.getString("g_name"));
            map.put("type", result.getString("g_type"));
            map.put("price", result.getString("g_price"));
            map.put("num", result.getString("g_num"));
            map.put("check", result.getString("g_check"));
            map.put("user", result.getString("user"));
            map.put("date", result.getString("date"));
            list.add(map);
        }
        return list;
    }

    public static List<HashMap<String, Object>> getOrder() throws SQLException {
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        Connection conn = getConnection();
        Statement sta = conn.createStatement();
//        String sql = "select * from myorder o1,waitorder o2 where o1.user='"+name+"'and o2.user='"+name+"'";
        String sql = "select * from myorder";
        ResultSet result = sta.executeQuery(sql);
        if (result == null) {
            return null;
        }
        while (result.next()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("id", result.getString("g_id"));
            map.put("photo", result.getString("g_photo"));
            map.put("name", result.getString("g_name"));
            map.put("type", result.getString("g_type"));
            map.put("price", result.getString("g_price"));
            map.put("user", result.getString("user"));
            map.put("num", result.getString("g_num"));
            map.put("address", result.getString("g_address"));
            map.put("user", result.getString("user"));
            map.put("date", result.getString("date"));
            list.add(map);
        }
        return list;
    }

    public static List<HashMap<String, Object>> getWaitorder(String name) throws SQLException {
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        Connection conn = getConnection();
        Statement sta = conn.createStatement();
//        String sql = "select * from myorder o1,waitorder o2 where o1.user='"+name+"'and o2.user='"+name+"'";
        String sql = "select * from waitorder where user='"+name+"'";
        ResultSet result = sta.executeQuery(sql);
        if (result == null) {
            return null;
        }
        while (result.next()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("id", result.getString("g_id"));
            map.put("photo", result.getString("g_photo"));
            map.put("name", result.getString("g_name"));
            map.put("type", result.getString("g_type"));
            map.put("price", result.getString("g_price"));
            map.put("num", result.getString("g_num"));
            map.put("check", result.getString("g_check"));
            map.put("user", result.getString("user"));
            map.put("date", result.getString("date"));
            list.add(map);
        }
        return list;
    }

    public static List<HashMap<String, Object>> getAdminname(String name) throws SQLException {
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        Connection conn = getConnection();
        Statement sta = conn.createStatement();
        String sql = "select * from admin where a_name='"+name+"'";
        ResultSet result = sta.executeQuery(sql);
        if (result == null) {
            return null;
        }
        while (result.next()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("id", result.getString("a_id"));
            map.put("name", result.getString("a_name"));
            map.put("psd", result.getString("a_psd"));
            list.add(map);
        }
        return list;
    }

    public static long allcartnum(int id, String name) throws SQLException {
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        Connection conn = getConnection();
        Statement sta = conn.createStatement();
        String sql = "select * from cart where g_id='"+id+"' and user='"+name+"'";
        ResultSet result = sta.executeQuery(sql);
        int count=0;
        while (result.next()) {
            count++;
        }
        return count;
    }

}
