package com.example.yuanann.vegetable_app;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yuanann.vegetable_app.cart.CartDBOpenHelper;
import com.example.yuanann.vegetable_app.home.Goods;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import android.support.v7.app.AppCompatActivity;

public class Mygoods extends AppCompatActivity {
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private ArrayList<Goods> goodsList;
    private CartDBOpenHelper dbOpenHelper;
    private static final String  TAG="DBUtils";
    String a=null;
    String b=null;
    String c =null;
    String d =null;
    long goodsnum;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mygoods);
        ImageView h1 = findViewById(R.id.h1);
        ImageView h4 = findViewById(R.id.h4);
        ImageView h5 = findViewById(R.id.h5);
        ImageView h6 = findViewById(R.id.h6);

        ImageView f1 = findViewById(R.id.f1);
        ImageView f2 = findViewById(R.id.f2);
        Button info=(Button)findViewById(R.id.info);
        TextView h2 = findViewById(R.id.h2);
        TextView h3 = findViewById(R.id.h3);

        final TextView tt1,tt2,tt3,tt4,ttt1,ttt2,ttt3,ttt4;
        final ImageView t1,t2,t3,t4;
        t1 =findViewById(R.id.t1);
        t2 =findViewById(R.id.t2);
        t3 =findViewById(R.id.t3);
        t4 =findViewById(R.id.t4);
        tt1 =findViewById(R.id.tt1);
        tt2 =findViewById(R.id.tt2);
        tt3 =findViewById(R.id.tt3);
        tt4 =findViewById(R.id.tt4);
        ttt1=findViewById(R.id.ttt1);
        ttt2 =findViewById(R.id.ttt2);
        ttt3 =findViewById(R.id.ttt3);
        ttt4 =findViewById(R.id.ttt4);
        TextView money = findViewById(R.id.money);
        TextView price = findViewById(R.id.price);
        ImageView g = findViewById(R.id.g);
        Button gm = findViewById(R.id.gm);
        Button gwc = findViewById(R.id.gwc);

        Intent intent = getIntent();
        int id = intent.getIntExtra("id", 1);
        SharedPreferences sp =Mygoods.this.getSharedPreferences("name", MODE_PRIVATE);
        String myname=sp.getString("name", null);
        String address =sp.getString("add", null);
        @SuppressLint("HandlerLeak")
        final Handler handler =new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                final GridView listView= findViewById(R.id.grid);
                List<HashMap<String,Object>> mlist= (List<HashMap<String, Object>>) msg.obj;
                Iterator<HashMap<String, Object>> its = mlist.iterator();
                String photo =null;
                String name =null;
                String type =null;
                String m =null;
                String[] descA = new String[mlist.size()];
                String[] descB = new String[mlist.size()];
                final String[] descC = new String[mlist.size()];
                final String[] descD = new String[mlist.size()];
                int p = 0;
                while(its.hasNext()){
                    HashMap<String, Object> map = new HashMap<String, Object>();
                    map=its.next();
                    photo= (String) map.get("photo");
                    descA[p] =photo;
                    name= (String) map.get("name");
                    descB[p] =name;
                    type= (String) map.get("type");
                    descC[p] =type;
                    m= (String) map.get("price");
                    descD[p] =m;
                    p++;
                }
                h1.setBackgroundResource(Integer.parseInt(descA[0]));
                h2.setText(descC[0]);
                h3.setText(descB[0]);
                price.setText( "￥" + descD[0] + "AED/500g");
                money.setText("￥" + descD[0] + "/500g");

                a=descA[0];
                b=descB[0];
                c=descC[0];
                d=descD[0];

            }
        };
        Thread thread=new Thread(new Runnable() {
            List<HashMap<String,Object>> list1=new ArrayList<HashMap<String, Object>>();
            @Override
            public void run() {
                Mydatabases.getConnection();
                try {
                    goodsnum=Mydatabases.allcartnum(id,myname);
                    list1=Mydatabases.getgoodsall(id);
                    Log.d(TAG, list1.toString());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Message message=Message.obtain();
                message.what=1;
                message.obj=list1;
                handler.sendMessage(message);
            }
        });
        thread.start();

        ImageView btn2 = findViewById(R.id.back);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Mygoods.this.finish();
            }
        });

        f1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(Mygoods.this, Home.class);
                startActivity(intent);
            }
        });
        f2.setOnClickListener(new View.OnClickListener() {
            Goodss toPayFragmentManager;
            @Override
            public void onClick(View view) {
            }
        });
        SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss ");
        Date curDate = new Date(System.currentTimeMillis());//获取当前时间
        String str = formatter.format(curDate);

        gm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //创建数据库
                CartDBOpenHelper dbOpenHelper = new CartDBOpenHelper(Mygoods.this,1);
                SQLiteDatabase db = dbOpenHelper.getWritableDatabase();

                if(myname==null) {
                    MyDialog2 myDialog = new MyDialog2(Mygoods.this, R.style.MyDialog);
                    myDialog.setTitle("Tips")
                            .setMessage("You are not logged in, do you want to log in now?")
                            .setCancel("cancel", dialog -> dialog.dismiss())
                            .setConfirm("yes", dialog -> {
                                dialog.dismiss();
                                Intent intent = new Intent();
                                intent.setClass(Mygoods.this, Login.class);
                                startActivity(intent);
                                Mygoods.this.finish();
                            })
                            .show();
                }
                else {
                    DatabaseMydapter database = new DatabaseMydapter();
                    database.onInsertWaieOrder(id+"", a, b, c, d, "1", "true", myname, str,address);
                    Intent intent = new Intent();
                    String dec = d;
                    String dec1 = id + "";
                    intent.setClass(Mygoods.this, Pay.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("id", dec);
                    bundle.putString("id2", dec1);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                }
        });

        gwc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long i = -1;
                //创建数据库
                CartDBOpenHelper dbOpenHelper = new CartDBOpenHelper(Mygoods.this,1);
                SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
                //商品数量
                int goods_num = isExist((id));
//                int goods_num = 0;
                if(myname==null) {
                    MyDialog2 myDialog = new MyDialog2(Mygoods.this, R.style.MyDialog);
                    myDialog.setTitle("Tips")
                            .setMessage("You are not logged in, do you want to log in now?")
                            .setCancel("cancel", dialog -> dialog.dismiss())
                            .setConfirm("yes", dialog -> {
                                dialog.dismiss();
                                Intent intent = new Intent();
                                intent.setClass(Mygoods.this, Login.class);
                                startActivity(intent);
                                Mygoods.this.finish();
                            })
                            .show();
                }
                else if (goodsnum == 0) {
                    Toast.makeText(Mygoods.this, b+ "Add to Cart successful！", Toast.LENGTH_SHORT).show();
                    DatabaseMydapter database=new DatabaseMydapter();
                    database.onInsertOrder(id+"",a,b,c,d,"1","true",myname,str,address);
                    ContentValues values = new ContentValues();
                    //cart(g_id,g_photo,g_name,g_type,g_price,g_num,g_check)
                    values.put("g_id", String.valueOf(id));
                    values.put("g_photo", String.valueOf(a));
                    values.put("g_name",b);
                    values.put("g_type", c);
                    values.put("g_price",d);
                    values.put("g_num", "1");
                    values.put("g_check", "false");
                    values.put("user",myname);
                    values.put("date", str);
                    i=db.insert("cart", null, values);
                    Intent intent = new Intent();
                    intent.setClass(Mygoods.this, Mygoods.class);
                    intent.putExtra("id",id);
                    startActivity(intent);
                    Mygoods.this.finish();
                } else {
                    Toast.makeText(Mygoods.this, b+ "Added to cart！", Toast.LENGTH_SHORT).show();
                    DatabaseMydapter database=new DatabaseMydapter();
                    long mynum=goodsnum+1;
                    database.onUpdateCart((int) mynum,myname,id);
                    ContentValues values = new ContentValues();
                    values.put("g_num", goodsnum + 1);
                    //参数:表名，修改后的值，where条件，以及约束，如果不指定三四两个参数，会更改所有行
                    i=dbOpenHelper.getWritableDatabase().update(
                            "cart",
                            values,
                            "g_id=?",
                            new String[]{String.valueOf(id)});
                    Intent intent = new Intent();
                    intent.setClass(Mygoods.this, Mygoods.class);
                    intent.putExtra("id",id);
                    startActivity(intent);
                    Mygoods.this.finish();
                }
            }

        });



        @SuppressLint("HandlerLeak")
        final Handler handler2 =new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                List<HashMap<String,Object>> mlist= (List<HashMap<String, Object>>) msg.obj;
                Iterator<HashMap<String, Object>> its = mlist.iterator();
                String photo =null;
                String name =null;
                String type =null;
                String m =null;
                String[] descA = new String[mlist.size()];
                String[] descB = new String[mlist.size()];
                final String[] descC = new String[mlist.size()];
                final String[] descD = new String[mlist.size()];
                int p = 0;
                while(its.hasNext()){
                    HashMap<String, Object> map = new HashMap<String, Object>();
                    map=its.next();
                    photo= (String) map.get("photo");
                    descA[p] =photo;
                    name= (String) map.get("name");
                    descB[p] =name;
                    type= (String) map.get("type");
                    descC[p] =type;
                    m= (String) map.get("price");
                    descD[p] =m;
                    p++;
                }
                t1.setBackgroundResource(Integer.parseInt(descA[2]));
                tt1.setText(descB[2]);
                ttt1.setText(descD[2]);
                t2.setBackgroundResource(Integer.parseInt(descA[1]));
                tt2.setText(descB[1]);
                ttt2.setText(descD[1]);
                t3.setBackgroundResource(Integer.parseInt(descA[0]));
                tt3.setText(descB[0]);
                ttt3.setText(descD[0]);
                t4.setBackgroundResource(Integer.parseInt(descA[3]));
                tt4.setText(descB[3]);
                ttt4.setText(descD[3]);

            }
        };
        Thread thread2=new Thread(new Runnable() {
            List<HashMap<String,Object>> list1=new ArrayList<HashMap<String, Object>>();
            @Override
            public void run() {
                Mydatabases.getConnection();
                try {
                    list1=Mydatabases.getgoodsall();
                    Log.d(TAG, list1.toString());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Message message=Message.obtain();
                message.what=1;
                message.obj=list1;
                handler2.sendMessage(message);
            }
        });
        thread2.start();

        t1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Intent intent = new Intent();
                intent.setClass(Mygoods.this, Mygoods.class);
                intent.putExtra("id",3);
                startActivity(intent);
            }
        });
        t2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Intent intent = new Intent();
                intent.setClass(Mygoods.this, Mygoods.class);
                intent.putExtra("id",2);
                startActivity(intent);
            }
        });

        t3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Intent intent = new Intent();
                intent.setClass(Mygoods.this, Mygoods.class);
                intent.putExtra("id",1);
                startActivity(intent);
            }
        });

        t4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Intent intent = new Intent();
                intent.setClass(Mygoods.this, Mygoods.class);
                intent.putExtra("id",4);
                startActivity(intent);
            }
        });
    }

    //判断商品id是否存在,存在返回商品数量，不存在返回0
    private int isExist(int g_id){
        dbOpenHelper = new CartDBOpenHelper(Mygoods.this,1);
        //表名，列名，where约束条件，where中占位符提供具体的值，指定group by的列，进一步约束
        Cursor cursor = dbOpenHelper.getReadableDatabase().query(
                "cart",
                new String[]{"g_num"},
                "g_id=?",
                new String[]{String.valueOf(g_id)},
                null,
                null,
                null);
        boolean idIsExist = cursor.moveToFirst();
        if (idIsExist){
            String str = cursor.getString(0);
            cursor.close();
            dbOpenHelper.close();
            //Toast.makeText(mContext,"当前数据库存在改商品！！！" + str, Toast.LENGTH_SHORT).show();
            return Integer.parseInt(str);
        }
        //Toast.makeText(mContext,"当前数据库不存在改商品！", Toast.LENGTH_SHORT).show();
        cursor.close();
        dbOpenHelper.close();
        return 0;
    }


}