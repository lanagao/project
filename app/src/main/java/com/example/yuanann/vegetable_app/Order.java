package com.example.yuanann.vegetable_app;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class Order extends AppCompatActivity implements MyAdapter.Callback, AdapterView.OnItemClickListener {
    private MyAdapter mMyAdapter;
    private static final String  TAG="DBUtils";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        final UnScrollListView list, list2, list3;
        list = findViewById(R.id.list);
        list2 = findViewById(R.id.list2);
        list3 = findViewById(R.id.list3);
        SharedPreferences sp =Order.this.getSharedPreferences("name", MODE_PRIVATE);
        String myname=sp.getString("name", null);
        TabHost tab = (TabHost) this.findViewById(android.R.id.tabhost);

        //初始化TabHost容器
        tab.setup();
        //在TabHost创建标签，然后设置：标题／图标／标签页布局
        tab.addTab(tab.newTabSpec("tab1").setIndicator("All", null).setContent(R.id.tab1));
        tab.addTab(tab.newTabSpec("tab2").setIndicator("Pending Delivery", null).setContent(R.id.tab2));
        tab.addTab(tab.newTabSpec("tab3").setIndicator("Pending Payment", null).setContent(R.id.tab3));
        tab.addTab(tab.newTabSpec("tab3").setIndicator("Pending Review", null).setContent(R.id.tab4));

        @SuppressLint("HandlerLeak")
        final Handler handler =new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                final ListView list = findViewById(R.id.list);
                final ListView list2 = findViewById(R.id.list2);
                final ListView list3 = findViewById(R.id.list3);
                if(msg.what==1){
                    List<HashMap<String,Object>> listview= (List<HashMap<String, Object>>) msg.obj;
                    SimpleAdapter simpleAdapter=new SimpleAdapter(Order.this,listview,R.layout.layout,new String[]{"photo","name","type","price","num","num","price"},new int[]{R.id.t1, R.id.t2, R.id.t3, R.id.t4, R.id.t5, R.id.t6, R.id.t7});
                    list.setAdapter(simpleAdapter);
                    list.postInvalidate();
                }
            }
        };
        Thread thread=new Thread(new Runnable() {
            List<HashMap<String,Object>> list1=new ArrayList<HashMap<String, Object>>();
            @Override
            public void run() {
                Mydatabases.getConnection();
                try {
                    list1=Mydatabases.getMyorder(myname);
                    Log.d(TAG, list1.toString());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Message message=Message.obtain();
                message.what=1;
                message.obj=list1;
                handler.sendMessage(message);
            }
        });
        thread.start();

        @SuppressLint("HandlerLeak")
        final Handler handler4 =new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                final ListView list = findViewById(R.id.list4);
                if(msg.what==1){
                    List<HashMap<String,Object>> listview= (List<HashMap<String, Object>>) msg.obj;
                    SimpleAdapter simpleAdapter=new SimpleAdapter(Order.this,listview,R.layout.layout,new String[]{"photo","name","type","price","num","num","price"},new int[]{R.id.t1, R.id.t2, R.id.t3, R.id.t4, R.id.t5, R.id.t6, R.id.t7});
                    list.setAdapter(simpleAdapter);
                    list.postInvalidate();
                }
            }
        };
        Thread thread4=new Thread(new Runnable() {
            List<HashMap<String,Object>> list1=new ArrayList<HashMap<String, Object>>();
            @Override
            public void run() {
                Mydatabases.getConnection();
                try {
                    list1=Mydatabases.getWaitorder(myname);
                    Log.d(TAG, list1.toString());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Message message=Message.obtain();
                message.what=1;
                message.obj=list1;
                handler4.sendMessage(message);
            }
        });
        thread4.start();


        @SuppressLint("HandlerLeak")
        final Handler handler2 =new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                final ListView list = findViewById(R.id.list2);
                if(msg.what==1){
                    List<HashMap<String,Object>> listview= (List<HashMap<String, Object>>) msg.obj;
                    SimpleAdapter simpleAdapter=new SimpleAdapter(Order.this,listview,R.layout.layout,new String[]{"photo","name","type","price","num","num","price"},new int[]{R.id.t1, R.id.t2, R.id.t3, R.id.t4, R.id.t5, R.id.t6, R.id.t7});
                    list.setAdapter(simpleAdapter);
                    list.postInvalidate();
                }
            }
        };
        Thread thread2=new Thread(new Runnable() {
            List<HashMap<String,Object>> list1=new ArrayList<HashMap<String, Object>>();
            @Override
            public void run() {
                Mydatabases.getConnection();
                try {
                    list1=Mydatabases.getMyorder(myname);
                    Log.d(TAG, list1.toString());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Message message=Message.obtain();
                message.what=1;
                message.obj=list1;
                handler2.sendMessage(message);
            }
        });
        thread2.start();


        @SuppressLint("HandlerLeak")
        final Handler handler3 =new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                final ListView list = findViewById(R.id.list3);
                if(msg.what==1){
                    List<HashMap<String,Object>> listview= (List<HashMap<String, Object>>) msg.obj;
                    SimpleAdapter simpleAdapter=new SimpleAdapter(Order.this,listview,R.layout.layout,new String[]{"photo","name","type","price","num","num","price"},new int[]{R.id.t1, R.id.t2, R.id.t3, R.id.t4, R.id.t5, R.id.t6, R.id.t7});
                    list.setAdapter(simpleAdapter);
                    list.postInvalidate();
                }
            }
        };
        Thread thread3=new Thread(new Runnable() {
            List<HashMap<String,Object>> list1=new ArrayList<HashMap<String, Object>>();
            @Override
            public void run() {
                Mydatabases.getConnection();
                try {
                    list1=Mydatabases.getWaitorder(myname);
                    Log.d(TAG, list1.toString());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Message message=Message.obtain();
                message.what=1;
                message.obj=list1;
                handler3.sendMessage(message);
            }
        });
        thread3.start();
    }
        @Override
        public void click(View v) {
            int position = (int) v.getTag();
            String clickItemText = mMyAdapter.getStringList().get(position);
            Toast.makeText(this, "button click -->"+clickItemText, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Toast.makeText(this,"listview item click -- >"
                    + mMyAdapter.getStringList().get(i), Toast.LENGTH_SHORT).show();
        }






}
