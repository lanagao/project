package com.example.yuanann.vegetable_app;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yuanann.vegetable_app.cart.CartDBOpenHelper;
import com.example.yuanann.vegetable_app.cart.DingdanActivity;
import com.example.yuanann.vegetable_app.cart.SuccessActivity;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

public class Pay extends AppCompatActivity {
    private TextView ding_money, ding_buy;
    private EditText u_address, u_name, u_phone;
    private CartDBOpenHelper dbOpenHelper;
    private static final String  TAG="DBUtils";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pay);
        Bundle bundle = this.getIntent().getExtras();
        String type=bundle.getString("id");
        String myid=bundle.getString("id2");
        int id=Integer.parseInt(myid);
        TextView textView=(TextView) findViewById(R.id.ding_money);
        TextView add=(TextView) findViewById(R.id.add);
        TextView ding_buy = findViewById(R.id.ding_buy);
        textView.setText("￥" +type);

        SharedPreferences sp =Pay.this.getSharedPreferences("name", MODE_PRIVATE);
        String b =sp.getString("name", null);
        String address =sp.getString("add", null);
        SimpleDateFormat formatter = new SimpleDateFormat ("yyyy年MM月dd日 HH:mm:ss ");
        Date curDate = new Date(System.currentTimeMillis());//获取当前时间
        String str = formatter.format(curDate);

        add.setText(address);
        ImageView btn2 = findViewById(R.id.back);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyDialog2 myDialog = new MyDialog2(Pay.this, R.style.MyDialog);
                myDialog.setTitle("Tips")
                        .setMessage("Are you sure to cancel the payment?")
                        .setCancel("cancel", dialog -> dialog.dismiss())
                        .setConfirm("yes", dialog -> {
                            DatabaseMydapter database = new DatabaseMydapter();
                            database.onUpdateWaitOrder();
                            Toast.makeText(Pay.this, "The product has been added to the pending payment order", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                            Pay.this.finish();
                        })
                        .show();
            }
        });

        RadioButton wei=findViewById(R.id.wei);
        RadioButton zhi=findViewById(R.id.zhi);
        RadioButton qq=findViewById(R.id.qq);
        wei.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zhi.setChecked(false);
                qq.setChecked(false);
            }
        });

        zhi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wei.setChecked(false);
                qq.setChecked(false);
            }
        });
        qq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wei.setChecked(false);
                zhi.setChecked(false);
            }
        });

        ding_buy.setOnClickListener(v -> {
            @SuppressLint("HandlerLeak")
            final Handler handler =new Handler(){
                @Override
                public void handleMessage(@NonNull Message msg) {
                    super.handleMessage(msg);
                    final GridView listView= findViewById(R.id.grid);
                    List<HashMap<String,Object>> mlist= (List<HashMap<String, Object>>) msg.obj;
                    Iterator<HashMap<String, Object>> its = mlist.iterator();
                    String a1 =null;
                    String a2 =null;
                    String a3 =null;
                    String a4 =null;
                    String a5 =null;
                    String a6 =null;
                    String a7 =null;
                    String a8 =null;
                    String a9 =null;
                    String[] descA = new String[mlist.size()];
                    String[] descB = new String[mlist.size()];
                    final String[] descC = new String[mlist.size()];
                    final String[] descD = new String[mlist.size()];
                    String[] descE = new String[mlist.size()];
                    String[] descF = new String[mlist.size()];
                    final String[] descG = new String[mlist.size()];
                    final String[] descH = new String[mlist.size()];
                    final String[] descI = new String[mlist.size()];
                    int p = 0;
                    while(its.hasNext()){
                        HashMap<String, Object> map = new HashMap<String, Object>();
                        map=its.next();
                        a1= (String) map.get("id");
                        descA[p] =a1;
                        a2= (String) map.get("photo");
                        descB[p] =a2;
                        a3= (String) map.get("name");
                        descC[p] =a3;
                        a4= (String) map.get("type");
                        descD[p] =a4;
                        a5= (String) map.get("price");
                        descE[p] =a5;
                        a6= (String) map.get("num");
                        descF[p] =a6;
                        a7= (String) map.get("check");
                        descG[p] =a7;
                        a8= (String) map.get("user");
                        descH[p] =a8;
                        a9= (String) map.get("date");
                        descI[p] =a9;
                        p++;
                    }
                    DatabaseMydapter database = new DatabaseMydapter();
                    database.onInsertMyOrder(a1,a2,a3,a4,a5,a6,a7,a8,a9,address);
                  }
            };
            Thread thread=new Thread(new Runnable() {
                List<HashMap<String,Object>> list1=new ArrayList<HashMap<String, Object>>();
                @Override
                public void run() {
                    Mydatabases.getConnection();
                    try {
                        list1=Mydatabases.getWaitcheck();
                        Log.d(TAG, list1.toString());
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    Message message=Message.obtain();
                    message.what=1;
                    message.obj=list1;
                    handler.sendMessage(message);
                }
            });
            thread.start();
            delCartAll();
            Toast.makeText(this, "The payment is being processed, please wait..", Toast.LENGTH_LONG).show();
            new Handler().postDelayed(() -> {
                startActivity(new Intent(this,SuccessActivity.class));
                //overridePendingTransition(android.R.anim.slide_in_left,android.R.anim.slide_out_right);
                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                finish();
            },3000);
        });
    }
    //删除购物车中选中的商品
    private void delCartAll(){
//        CartDBOpenHelper dbOpenHelper = new CartDBOpenHelper(this, 1);
        CartDBOpenHelper dbOpenHelper = new CartDBOpenHelper(this,1);
        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
        //参数依次是表名，以及where条件与约束
        dbOpenHelper.getWritableDatabase().delete(
                "waitorder",
                "g_check=?",
                new String[]{"true"});
        dbOpenHelper.close();
    }
    //修改商品是否被购买
    private void updateWaitvege(int g_id){
        dbOpenHelper = new CartDBOpenHelper(this, 1);
        ContentValues values = new ContentValues();
        values.put("g_check","false");
        //参数:表名，修改后的值，where条件，以及约束，如果不指定三四两个参数，会更改所有行
        dbOpenHelper.getWritableDatabase().update(
                "waitorder",
                values,
                "g_id=?",
                new String[]{String.valueOf(g_id)});
        dbOpenHelper.close();
    }

}
