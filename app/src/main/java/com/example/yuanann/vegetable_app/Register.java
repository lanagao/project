package com.example.yuanann.vegetable_app;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

public class Register extends AppCompatActivity {
    private static final String  TAG="DBUtils";
    Button bu1;
    EditText ed1,ed2,ed3;
    String name,psw,pswagain;
    String a="n";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        bu1 = (Button) findViewById(R.id.bu1);
        ed1=(EditText) findViewById(R.id.ed1);
        ed2=(EditText) findViewById(R.id.ed2);
        ed3=(EditText) findViewById(R.id.ed3);
        bu1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                name = ed1.getText().toString().trim();
                psw= ed2.getText().toString().trim();
                pswagain= ed3.getText().toString().trim();
                @SuppressLint("HandlerLeak")
                final Handler handler2 =new Handler(){
                    @Override
                    public void handleMessage(@NonNull Message msg) {
                        super.handleMessage(msg);
                        final ListView listView=Register.this.findViewById(R.id.list);
                        List<HashMap<String,Object>> mlist= (List<HashMap<String, Object>>) msg.obj;
                        Iterator<HashMap<String, Object>> its = mlist.iterator();
                        String id =null;
                        final String[] descD = new String[mlist.size()];
                        int p = 0;
                        while(its.hasNext()){
                            HashMap<String, Object> map = new HashMap<String, Object>();
                            map=its.next();
                            id= (String) map.get("id");
                            descD[p] =id;
                            p++;
                        }
                        if(p==0){

                        }else{
                            a="y";
                        }
                        if (TextUtils.isEmpty(name)) {
                            Toast.makeText( Register.this, "Please enter username", Toast.LENGTH_SHORT).show();
                        } else if (TextUtils.isEmpty(psw)) {
                            Toast.makeText( Register.this, "Please enter password", Toast.LENGTH_SHORT).show();
                        } else if (TextUtils.isEmpty(pswagain)) {
                            Toast.makeText( Register.this,"Please enter password again", Toast.LENGTH_SHORT).show();
                        } else if (!psw.equals(pswagain)) {
                            Toast.makeText(Register.this, "The password entered twice is different", Toast.LENGTH_SHORT).show();
                        }else if(a.equals("y")){
                            Toast.makeText(Register.this, "This username has already existed", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(Register.this, "register successful", Toast.LENGTH_SHORT).show();
                            Intent data = new Intent();
                            setResult(RESULT_OK, data);
                            SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss ");
                            Date curDate = new Date(System.currentTimeMillis());//获取当前时间
                            String str = formatter.format(curDate);

                            DatabaseMydapter database=new DatabaseMydapter();
                            database.onInsertUser(name,psw,str);
                            Register.this.finish();
                        }

                    }
                };
                Thread thread2=new Thread(new Runnable() {
                    List<HashMap<String,Object>> list1=new ArrayList<HashMap<String, Object>>();
                    @Override
                    public void run() {
                        Mydatabases.getConnection();
                        try {
                            list1=Mydatabases.geUsername(name);
                            Log.d(TAG, list1.toString());
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        Message message=Message.obtain();
                        message.what=1;
                        message.obj=list1;
                        handler2.sendMessage(message);
                    }
                });
                thread2.start();

            } });
    }

}

