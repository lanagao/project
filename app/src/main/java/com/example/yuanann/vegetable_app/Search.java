package com.example.yuanann.vegetable_app;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class Search extends AppCompatActivity {
    private static final String  TAG="DBUtils";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        final UnScrollListView list2;
        list2= findViewById(R.id.list);
        Intent intent = getIntent();
        final String name = intent.getStringExtra("search");

        @SuppressLint("HandlerLeak")
        final Handler handler =new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                final ListView listView= findViewById(R.id.list);
                List<HashMap<String,Object>> mlist= (List<HashMap<String, Object>>) msg.obj;
                Iterator<HashMap<String, Object>> its = mlist.iterator();
                String id =null;
                final String[] descD = new String[mlist.size()];
                int p = 0;
                while(its.hasNext()){
                    HashMap<String, Object> map = new HashMap<String, Object>();
                    map=its.next();
                    id= (String) map.get("id");
//                    System.out.print(id+"\n");
                    descD[p] =id;
                    p++;
                }
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        int cl= Integer.parseInt((descD[i]));
                        Intent intent=new Intent(Search.this,Mygoods.class);
                        intent.putExtra("id",cl);
                        startActivity(intent);
                    }
                });
                if(msg.what==1){
                    List<HashMap<String,Object>> list2= (List<HashMap<String, Object>>) msg.obj;
                    SimpleAdapter simpleAdapter=new SimpleAdapter(Search.this,list2,R.layout.layout2,new String[]{"photo","name","type","price"},new int[]{R.id.t1,R.id.t2,R.id.t3,R.id.t4});
                    listView.setAdapter(simpleAdapter);
                }
            }
        };
        Thread thread=new Thread(new Runnable() {
            List<HashMap<String,Object>> list1=new ArrayList<HashMap<String, Object>>();
            @Override
            public void run() {
                Mydatabases.getConnection();
                try {
                    list1=Mydatabases.getSearch(name);
                    Log.d(TAG, list1.toString());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Message message=Message.obtain();
                message.what=1;
                message.obj=list1;
                handler.sendMessage(message);
            }
        });
        thread.start();

    }


}
