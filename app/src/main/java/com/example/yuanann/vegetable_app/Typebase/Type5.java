package com.example.yuanann.vegetable_app.Typebase;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.example.yuanann.vegetable_app.DatabaseHelper;
import com.example.yuanann.vegetable_app.Mygoods;
import com.example.yuanann.vegetable_app.R;
import com.example.yuanann.vegetable_app.base.BaseFragment;
import com.example.yuanann.vegetable_app.home.Goods;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;


public class Type5 extends BaseFragment {
    private static final String  TAG="DBUtils";
    private ListView list;
//    int a=0;
    @Override
    public View initView() {
        View view = View.inflate(mContext, R.layout.type_grid, null);
        list=view.findViewById(R.id.list2);
        initListener();
        return view;
    }

    public void initData(){
        super.initData();
        ArrayList<Goods> goods = new ArrayList<>();
        SharedPreferences sp =  getActivity().getSharedPreferences("name", MODE_PRIVATE);
        final String name =sp.getString("name", null);
        DatabaseHelper helper = new DatabaseHelper(getContext());
        Cursor cursor=helper.queryMygoods_type10();

        final int[] descD = new int[cursor.getCount()];
        int p=0;
        while(cursor.moveToNext()) {
            int a2 = cursor.getInt(cursor.getColumnIndex("_id"));
            descD[p] = a2;
            p++;
        }

//        定义SimpleCursorAdapter
        SimpleCursorAdapter simpleAdapter=new SimpleCursorAdapter(getContext(),R.layout.type_grid_item1,cursor,new String[]{"g_photo","g_name","g_type","g_price"},new int[]{R.id.a1,R.id.a2,R.id.a3,R.id.a4});
        //设置适配器
        list.setAdapter(simpleAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                int cl=descD[i];
                Intent intent=new Intent(getContext(),Mygoods.class);
                intent.putExtra("id",cl);
                startActivity(intent);
            }
        });
    }
    private void initListener(){
    }
}
