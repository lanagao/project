package com.example.yuanann.vegetable_app.Typebase;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;

import com.example.yuanann.vegetable_app.DatabaseHelper;
import com.example.yuanann.vegetable_app.Mydatabases;
import com.example.yuanann.vegetable_app.Mygoods;
import com.example.yuanann.vegetable_app.R;
import com.example.yuanann.vegetable_app.base.BaseFragment;
import com.example.yuanann.vegetable_app.home.Goods;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

;

public class Xihongshi3 extends BaseFragment {
    private static final String  TAG="DBUtils";
    private ListView list;
    //    int a=0;
    @Override
    public View initView() {
        View view = View.inflate(mContext, R.layout.type_grid, null);
        list=view.findViewById(R.id.list2);
        initListener();
        return view;
    }

    public void initData(){
        super.initData();
        ArrayList<Goods> goods = new ArrayList<>();
        SharedPreferences sp =  getActivity().getSharedPreferences("name", MODE_PRIVATE);
        final String name =sp.getString("name", null);
        @SuppressLint("HandlerLeak")
        final Handler handler =new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                List<HashMap<String,Object>> mlist= (List<HashMap<String, Object>>) msg.obj;
                Iterator<HashMap<String, Object>> its = mlist.iterator();
                String id =null;
                final String[] descD = new String[mlist.size()];
                int p = 0;
                while(its.hasNext()){
                    HashMap<String, Object> map = new HashMap<String, Object>();
                    map=its.next();
                    id= (String) map.get("id");
//                    System.out.print(id+"\n");
                    descD[p] =id;
                    p++;
                }
                list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        int cl= Integer.parseInt((descD[i]));
                        Intent intent=new Intent(getContext(),Mygoods.class);
                        intent.putExtra("id",cl);
                        startActivity(intent);
                    }
                });
                if(msg.what==1){
                    List<HashMap<String,Object>> list2= (List<HashMap<String, Object>>) msg.obj;
                    SimpleAdapter simpleAdapter=new SimpleAdapter(getContext(),list2,R.layout.type_grid_item1,new String[]{"photo","name","type","price"},new int[]{R.id.a1,R.id.a2,R.id.a3,R.id.a4});
                    list.setAdapter(simpleAdapter);
                }
            }
        };
        Thread thread=new Thread(new Runnable() {
            List<HashMap<String,Object>> list1=new ArrayList<HashMap<String, Object>>();
            @Override
            public void run() {
                Mydatabases.getConnection();
                try {
                    list1=Mydatabases.getgoods5();
                    Log.d(TAG, list1.toString());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Message message=Message.obtain();
                message.what=1;
                message.obj=list1;
                handler.sendMessage(message);
            }
        });
        thread.start();
    }

    private void initListener(){

    }
}
