package com.example.yuanann.vegetable_app;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewTreeObserver;
import android.widget.ListView;

public class UnScrollListView1 extends ListView {

    public UnScrollListView1(Context context) {
        super(context);
    }

    public UnScrollListView1(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public UnScrollListView1(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
                MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);//这里返回的是刚写好的expandSpec

    }
}
