package com.example.yuanann.vegetable_app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TabHost;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

public class Vary extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.activity_vary,container,false);
        return view;
    }
    private CustomVideoView videoview;
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        super.onCreate(savedInstanceState);
        ImageView p1=getActivity().findViewById(R.id.m1);
        ImageView p2=getActivity().findViewById(R.id.m2);
        ImageView p3=getActivity().findViewById(R.id.m3);
        ImageView p4=getActivity().findViewById(R.id.m4);
        ImageView p5=getActivity().findViewById(R.id.m5);
        ImageView p6=getActivity().findViewById(R.id.m6);
        ImageView p7=getActivity().findViewById(R.id.m7);
        ImageView p8=getActivity().findViewById(R.id.m8);
        ImageView p9=getActivity().findViewById(R.id.m9);
        ImageView p10=getActivity().findViewById(R.id.m10);
        ImageView p11=getActivity().findViewById(R.id.m11);
        ImageView p12=getActivity().findViewById(R.id.m12);

        p1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(),Myfind.class);
                intent.putExtra("count",0);
                startActivity(intent);
            }
        });
        p2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(),Myfind.class);
                intent.putExtra("count",1);
                startActivity(intent);
            }
        });
        p3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(),Myfind.class);
                intent.putExtra("count",2);
                startActivity(intent);
            }
        });
        p4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(),Myfind.class);
                intent.putExtra("count",3);
                startActivity(intent);
            }
        });
        p5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(),Myfind.class);
                intent.putExtra("count",4);
                startActivity(intent);
            }
        });
        p6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(),Myfind.class);
                intent.putExtra("count",5);
                startActivity(intent);
            }
        });
        p7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(),Myfind.class);
                intent.putExtra("count",6);
                startActivity(intent);
            }
        });
        p6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(),Myfind.class);
                intent.putExtra("count",5);
                startActivity(intent);
            }
        });
        p7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(),Myfind.class);
                intent.putExtra("count",6);
                startActivity(intent);
            }
        });
        p8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(),Myfind.class);
                intent.putExtra("count",7);
                startActivity(intent);
            }
        });
        p7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(),Myfind.class);
                intent.putExtra("count",6);
                startActivity(intent);
            }
        });
        p8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(),Myfind.class);
                intent.putExtra("count",7);
                startActivity(intent);
            }
        });
        p9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(),Myfind.class);
                intent.putExtra("count",8);
                startActivity(intent);
            }
        });
        p10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(),Myfind.class);
                intent.putExtra("count",9);
                startActivity(intent);
            }
        });
        p11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(),Myfind.class);
                intent.putExtra("count",10);
                startActivity(intent);
            }
        });
        p12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(),Myfind.class);
                intent.putExtra("count",11);
                startActivity(intent);
            }
        });

        TabHost tab = (TabHost) getActivity().findViewById(android.R.id.tabhost);

        //初始化TabHost容器
        tab.setup();
        //在TabHost创建标签，然后设置：标题／图标／标签页布局
        tab.addTab(tab.newTabSpec("tab1").setIndicator("Recommendation", null).setContent(R.id.tab1));
        tab.addTab(tab.newTabSpec("tab2").setIndicator("For autumn", null).setContent(R.id.tab2));
        tab.addTab(tab.newTabSpec("tab3").setIndicator("Cook everyday", null).setContent(R.id.tab3));

        Fragment video = new Video();
        //实例化管理器
        FragmentManager fragmentManager1 = getFragmentManager();
        //定义事务
        FragmentTransaction fta1 = fragmentManager1.beginTransaction();
        //添加fragment
        fta1.add(R.id.varyfag, video);
        //提交事务`
        fta1.commit();

    }


}




