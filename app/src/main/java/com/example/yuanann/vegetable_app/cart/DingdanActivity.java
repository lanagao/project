package com.example.yuanann.vegetable_app.cart;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yuanann.vegetable_app.DatabaseHelper;
import com.example.yuanann.vegetable_app.DatabaseMydapter;
import com.example.yuanann.vegetable_app.Home;
import com.example.yuanann.vegetable_app.MyDialog2;
import com.example.yuanann.vegetable_app.R;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DingdanActivity extends AppCompatActivity {

    private TextView ding_money, ding_buy;
    private EditText u_address, u_name, u_phone;
    private CartDBOpenHelper dbOpenHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dingdan);

        ding_money = findViewById(R.id.ding_money);
        ding_buy = findViewById(R.id.ding_buy);
        TextView add=(TextView) findViewById(R.id.add);
        Intent intent = getIntent();
        double money = intent.getDoubleExtra("money", 0.0);
        ding_money.setText("" + money);

        SharedPreferences sp =DingdanActivity.this.getSharedPreferences("name", MODE_PRIVATE);
        String b =sp.getString("name", null);
        String address =sp.getString("add", null);
        SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss ");
        Date curDate = new Date(System.currentTimeMillis());//获取当前时间
        String str = formatter.format(curDate);
        add.setText(address);
        RadioButton wei=findViewById(R.id.wei);
        RadioButton zhi=findViewById(R.id.zhi);
        RadioButton qq=findViewById(R.id.qq);
        wei.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zhi.setChecked(false);
            }
        });

        zhi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wei.setChecked(false);
            }
        });
        qq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wei.setChecked(false);
                zhi.setChecked(false);
            }
        });

        ImageView btn2 = findViewById(R.id.back);
        btn2.setOnClickListener(v -> {
                MyDialog2 myDialog = new MyDialog2(DingdanActivity.this, R.style.MyDialog);
                myDialog.setTitle("Tips")
                        .setMessage("Are you sure to cancelthe payment?")
                        .setCancel("cancel", dialog -> dialog.dismiss())
                        .setConfirm("yes", dialog -> {
                            dbOpenHelper = new CartDBOpenHelper(DingdanActivity.this, 1);
                            Cursor cursor = dbOpenHelper.getReadableDatabase().query("cart", null, "g_check=?", new String[]{"true"}, null, null, null);
                            int a = 0;
                            while (cursor.moveToNext()) {
                                String a1=String.valueOf(Integer.parseInt(cursor.getString(1)));
                                String a2=String.valueOf(String.valueOf(Integer.parseInt(cursor.getString(2))));
                                String a3=cursor.getString(3);
                                String a4=cursor.getString(4);
                                String a5=String.valueOf( Double.parseDouble(cursor.getString(5)));
                                String a6=String.valueOf(Integer.parseInt(cursor.getString(6)));
                                DatabaseMydapter database = new DatabaseMydapter();
                                database.onInsertWaiteOrder(a1,a2,a3,a4,a5,a6,"false",b,str,address);
                                database.onDeletecartgoods(b,a1);
                            }
                            delCartAll();
                            Toast.makeText(DingdanActivity.this, "The product has been added to the pending payment order", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                            intent.setClass(DingdanActivity.this,Home.class);
                            startActivity(intent);
                        })
                        .show();
        });

        ding_buy.setOnClickListener(v -> {
            dbOpenHelper = new CartDBOpenHelper(DingdanActivity.this,1);
            Cursor cursor = dbOpenHelper.getReadableDatabase().query("cart",null,"g_check=?",new String[]{"true"},null,null,null);
            int a=0;
                    while (cursor.moveToNext()) {
//                        DatabaseHelper dbHelper = new DatabaseHelper(this);
//                        ContentValues values = new ContentValues();
                        String a1=String.valueOf(Integer.parseInt(cursor.getString(1)));
                        String a2=String.valueOf(String.valueOf(Integer.parseInt(cursor.getString(2))));
                        String a3=cursor.getString(3);
                        String a4=cursor.getString(4);
                        String a5=String.valueOf( Double.parseDouble(cursor.getString(5)));
                        String a6=String.valueOf(Integer.parseInt(cursor.getString(6)));
                        String a7= cursor.getString(7);
                        DatabaseMydapter database = new DatabaseMydapter();
                        database.onInsertMyOrder(a1,a2,a3,a4,a5,a6,a7,b,str,address);
                        database.onDeletecartgoods(b,a1);
                    }
            delCartAll();
            Toast.makeText(this, "The payment is being processed, please wait..", Toast.LENGTH_LONG).show();
            new Handler().postDelayed(() -> {
                startActivity(new Intent(this,SuccessActivity.class));
                //overridePendingTransition(android.R.anim.slide_in_left,android.R.anim.slide_out_right);
                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                finish();
            },3000);
        });
    }
    //删除购物车中选中的商品
    private void delCartAll(){
//        CartDBOpenHelper dbOpenHelper = new CartDBOpenHelper(this, 1);
        CartDBOpenHelper dbOpenHelper = new CartDBOpenHelper(this,1);
        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
        //参数依次是表名，以及where条件与约束
        dbOpenHelper.getWritableDatabase().delete(
                "cart",
                "g_check=?",
                new String[]{"true"});
        dbOpenHelper.close();
    }
}
